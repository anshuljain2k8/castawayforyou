//Capture Functionality
var pictureSource; // picture source
var destinationType; // sets the format of returned value
var filesRoot = "http://www.mumbaicolors.com/sites/default/files";
var baseurl = "http://www.mumbaicolors.com";

//Constants for creating folder castaway
var buttomDom;
var statusDom;
var fileSystem;
var device;
var drawerStatus = false;
var isTravPhotos = false;
var isCamera = false;
var firsttime = true;
var firsttimeflag = false;
var isFbLogin = false;
var isGoogleLogin = false;
var isTwitterLogin = false;
var fbuserobj = {};
var fbobj = {};
var interval;
var loggedInUser = '';

$(document).on('pageinit', function (event) {
    $(document).on("keyup", "#filter", function (event) {
        var filter = $(this).val(),
            count = 0;
        $(".stationlist h2").each(function () {
            var parent = $(this).parent().parent().parent(),
                length = $(this).text().length > 0;
            if (length && $(this).text().search(new RegExp(filter, "i")) < 0) {
                parent.hide();
            } else {
                parent.show();
                count++;
            }
        });
    });

    $(document).on("keyup", "#filtermonths", function (event) {
        var filter = $(this).val(),
            count = 0;
        $(".monthslist h2").each(function () {
            var parent = $(this).parent().parent().parent(),
                length = $(this).text().length > 0;
            if (length && $(this).text().search(new RegExp(filter, "i")) < 0) {
                parent.hide();
            } else {
                parent.show();
                count++;
            }
        });
    });

    $(document).on("keyup", "#filterattractions", function (event) {
        var filter = $(this).val(),
            count = 0;
        $(".attractionslist h2").each(function () {
            var parent = $(this).parent().parent().parent(),
                length = $(this).text().length > 0;
            if (length && $(this).text().search(new RegExp(filter, "i")) < 0) {
                parent.hide();
            } else {
                parent.show();
                count++;
            }
        });
    });

    $(document).on("keyup", "#filterregions", function (event) {
        var filter = $(this).val(),
            count = 0;
        $(".regionslist h2").each(function () {
            var parent = $(this).parent().parent().parent(),
                length = $(this).text().length > 0;
            if (length && $(this).text().search(new RegExp(filter, "i")) < 0) {
                parent.hide();
            } else {
                parent.show();
                count++;
            }
        });
    });

    $(document).on("keyup", "#filtertime", function (event) {
        var filter = $(this).val(),
            count = 0;
        $(".stationtimelist h2").each(function () {
            var parent = $(this).parent().parent().parent(),
                length = $(this).text().length > 0;
            if (length && $(this).text().search(new RegExp(filter, "i")) < 0) {
                parent.hide();
            } else {
                parent.show();
                count++;
            }
        });
    });
});

// Wait for Cordova to load
document.addEventListener("deviceready", onDeviceReady, false);

window.addEventListener('load', function () {
    setTimeout(function () {
        window.scrollTo(0, 0);
    }, 100);
    $('body').append("<div class='ui-loader-background'> </div>");
}, true);

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    device = "mobile";
} else {
    device = "desktop";
}

function forceGpsSet() {
    var positionOption = {
        timeout: 500,
        enableHighAccuracy: true
    };
    navigator.geolocation.getCurrentPosition(setForceGpsPosition, showForceError, positionOption);
}

function setForceGpsPosition(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;

    localStorage.setItem('latitude', lat);
    localStorage.setItem('longitude', lng);

    var distance = localStorage.getItem('default_distance');
    $("#popupBasic").popup("close");
    $.mobile.changePage("#showmap");
}

function showForceError(error) {
    var lat = '19.076'; //Default latitude of mumbai;
    var lng = '72.8777'; // Default Longitude of mumbai;

    var distance = localStorage.getItem('default_distance');
    createloadingmsg('Getting default location data');
    loadNearestStations(lat, lng, distance);
    $("#popupBasic").popup("close");
}

function initializeGpsLocation() {
    if (navigator.geolocation) {
        var lat = localStorage.getItem('latitude');
        var lng = localStorage.getItem('longitude');

        if (lat == null && lng == null) {
            navigator.geolocation.getCurrentPosition(setGpsPosition, showError);
        }
    }
}

function setGpsPosition(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;

    localStorage.setItem('latitude', lat);
    localStorage.setItem('longitude', lng);
}

function showError(error) {
    var lat = '19.076'; //Default latitude of mumbai;
    var lng = '72.8777'; // Default Longitude of mumbai;

    localStorage.setItem('latitude', lat);
    localStorage.setItem('longitude', lng);
}

function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var radlon1 = Math.PI * lon1 / 180
    var radlon2 = Math.PI * lon2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") {
        dist = dist * 1.609344
    }
    if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

// Cordova is ready
function onDeviceReady() {

    isCamera = true;
    document.addEventListener("backbutton", onBackKeyDown, false);

    navigator.splashscreen.hide();
    pictureSource = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;

    try {
        var pushNotification = window.plugins.pushNotification;
        pushNotification.register(successHandler, errorHandler, {
            "senderID": "375224829816",
            "ecb": "onNotificationGCM"
        });
    } catch (ex) {
        console.log('error: ' + ex);
    }

    checkConnection();

    navigator.geolocation.getCurrentPosition(geolocationSuccess, onError);

    var isDir = localStorage.getItem('direcreated');

    //Function to create directory cast away in sdcard root
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
        function (fs) {
            fileSystem = fs;

            var sdCardPath = fileSystem.root.toURL();
            localStorage.setItem('sdcardpath', sdCardPath);

            // if(isDir !== 'true'){
            var entry = fileSystem.root;
            // Create directory for info files
            entry.getDirectory("castaway", {
                create: true,
                exclusive: false
            }, onGetDirectorySuccess, onGetDirectoryFail);
            // }
        },
        function (e) {
            $.mobile.changePage("#homepage");
        });
        var filepath = localStorage.getItem('sdcardpath');
        var fullFilePath = filepath + 'castaway/data/cache/all_categories.json';
        window.resolveLocalFileSystemURL(fullFilePath, checkfilesuccess, checkfilefail);        
}

function checkfilesuccess(){
    // All cache files exist then dont do anything.
}

function checkfilefail(){
    var filepath = localStorage.getItem('sdcardpath');
    var fullOfflinePath = filepath + 'castaway/offline.txt';
    window.resolveLocalFileSystemURL(fullOfflinePath, 
    function(fileEntry){
        fileEntry.file(function(file) {
            var reader = new FileReader();
            reader.onloadend = function(e) {
               if( this.result == 'on') {
                    $("#goto_offline").popup("option", "overlayTheme", "a");
                    setTimeout(function () {
                        $("#goto_offline").popup("open");
                    }, 100); 
               }
            }
            reader.readAsText(file);
        });
    }, function(error){

    }); 
}

function onGetDirectorySuccess(fileEntry) {
    localStorage.setItem('direcreated', true);
    fileEntry.getFile("offline.txt", {create: true, exclusive: true}, gotFile);

    // Create directory for data files
    fileEntry.getDirectory("data", {
        create: true,
        exclusive: false
    }, function (success) {
        localStorage.setItem('datadircreated', true);
    }, function (error) {
    });
}

function onGetDirectoryFail(error) {
    console.log("Error creating directory " + error.code);
}

function gotFile(fileEntry) {
    // Do something with fileEntry here
    fileEntry.createWriter(gotFileWriter, failFileWriter);
}

function gotFileWriter(writer) {
    var initialSetting = 'off';
    writer.write(initialSetting);
}

function failFileWriter(error) {
    console.log("error : " + error.code);
}

function successHandler(result) {
    //alert('Callback Success! Result = '+result);
}

function errorHandler(error) {
    //alert(error);
}

function checkIfFileExists(path){
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){
        fileSystem.root.getFile(path, { create: false }, fileExists, fileDoesNotExist);
    }, getFSFail); //of requestFileSystem
}
function fileExists(fileEntry){
    alert("File " + fileEntry.fullPath + " exists!");
}
function fileDoesNotExist(){
    alert("file does not exist");
}
function getFSFail(evt) {
    console.log(evt.target.error.code);
}

function onNotificationGCM(e) {
    switch (e.event) {
        case 'registered':
            if (e.regid.length > 0) {
                if (e.regid !== $.trim(localStorage.getItem('gcmkey'))) {
                    localStorage.setItem('gcmkey', e.regid);
                    saveGcmKey(e.regid);
                }
            }
            break;

        case 'message':
            // this is the actual push notification. its format depends on the data model from the push server
            //console.log('message = '+e.message+' msgcnt = '+e.msgcnt);
            if (e.foreground) {
                // ECB message event come here when your app is in foreground(not in background)
            } else {
                if (e.coldstart) {
                    if (e.payload.id !== undefined || e.payload.id !== null) {
                        $.mobile.changePage('#' + e.payload.id);
                    }
                } else {
                    // App at background.
                }
            }
            break;

        case 'error':
            console.log('GCM error = ' + e.msg);
            break;

        default:
            console.log('An unknown GCM event has occurred');
            break;
    }
}

function saveGcmKey(gcmkey) {
    var gcmkeylocal = localStorage.getItem('gcmkey');
    if ($.trim(gcmkey) !== '' && gcmkeylocal !== '') {
        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=pushgcmkey&gcm_key=' + gcmkey,
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.mobile.changePage("#homepage");
            },
            success: function (data) {
                // Saved
                //Status of registration message pushed , true if pushed successfully, false if already exist.
            }
        });
    }
}

//Handle the back button
function onBackKeyDown() {
    if ($.mobile.activePage.attr('id') == "homepage" || $.mobile.activePage.attr('id') == "loginpage") {
        navigator.app.exitApp();
    } else {
        navigator.app.backHistory();
    }

    if ($.mobile.activePage.attr('id') == "offlineapp") {
        navigator.app.exitApp();
    }
}

$(document).on('vclick', '#startDl', function (e) {
    $("#networkdownload").popup("option", "overlayTheme", "a");
    setTimeout(function () {
        $("#networkdownload").popup("open");
    }, 100);
});

$(document).on('vclick', '#bkyes', function (e) {
    $("#networkdownload").popup("close");
    var url = filesRoot + "/versions/data.zip";
    var filename = "castaway/data/data.zip";
    loading(url, filename);
});

$(document).on('vclick', '#bkno', function (e) {
    $("#networkdownload").popup("close");
    $.mobile.changePage("#offline");
});

$(document).on('vclick', '#gtoyes', function (e) {
    $.mobile.changePage("#offline");
});

$(document).on('vclick', '#gtono', function (e) {
    $("#goto_offline").popup("close");
});

function loading(url, filename) {
    // Open popup for download progress.
    $("#downloading").popup("option", "overlayTheme", "a");
    setTimeout(function () {
        $("#downloading").popup("open");
    }, 100);

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var fileTransfer = new FileTransfer();

        var statusDom = $('#status');

        fileTransfer.onprogress = function (progressEvent) {
            if (progressEvent.lengthComputable) {
                var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
                $('#downloadStatus').html(perc + "% downloaded...");
            } else {
                if (statusDom.html() == "") {
                    statusDom.html("Loading");
                } else {
                    statusDom.append(".");
                }
            }
        };

        fileTransfer.download(
            url,
                fileSystem.root.toURL() + '/' + filename,
            function (entry) {
                // Write logic here for unzipping.
                $("#downloading").popup("close");
                // Full path to the zip file
                var sdCardPath = fileSystem.root.toURL();
                var path = sdCardPath + 'castaway/data/data.zip';
                zip.unzip(path, sdCardPath + 'castaway/data', function (complete) {
                    fileSystem.root.getDirectory("castaway", {
                        create: true,
                        exclusive: false
                    }, function (folderEntry) {
                        folderEntry.getDirectory("data", {
                            create: true,
                            exclusive: false
                        }, function (success) {
                            success.getFile("data.zip", {create: false, exclusive: false},
                                function (fileEntry) {
                                    fileEntry.remove(
                                        function (success) {
                                        }
                                        , function (error) {
                                        });
                                }
                                , function (error) {
                                });
                        }, function (error) {
                        });
                    }, function (error) {
                    });
                });
            },
            function (error) {
            }
        );
    });
}


// Set content offline or online for independent users.
$(document).on('change', '#flip-3', function () {
    var offline_status = $(this).val();
    var filepath = localStorage.getItem('sdcardpath');
    $.ajax({
        url: filepath + 'castaway/offline.txt',
        type: 'get',
        dataType: 'text',
        success: function (data) {
            writeToFile(offline_status);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {},
    });
    localStorage.setItem('offlineContent', offline_status);
});

function writeToFile(status) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getDirectory("castaway", {create: false}, function (dir) {
            dir.getFile("offline.txt", {create: false}, function (file) {
                ;
                file.createWriter(function (fileWriter) {
                    var rootPath = fileSystem.root.toURL();
                    fileWriter.write(status);
                    localStorage.setItem('offlineStatus', status);
                }, fail);
            });
        });
    });
}

function geolocationSuccess(position) {
    localStorage.setItem('latitude', position.coords.latitude);
    localStorage.setItem('longitude', position.coords.longitude);

    var lat = localStorage.getItem('latitude');
    var lng = localStorage.getItem('longitude');

    //If Found latitude is not updated one
    if (lat !== position.coords.latitude) {
        localStorage.setItem('latitude', position.coords.latitude);
    }

    //If found longitude is not updated one
    if (lng !== position.coords.longitude) {
        localStorage.setItem('longitude', position.coords.longitude);
    }

}

// onError Callback receives a PositionError object
function onError(error) {
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
}

function checkConnection() {
    var networkState = navigator.network.connection.type;
    var states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.NONE] = 'No network connection';

    localStorage.setItem('networkState', networkState);

    // If there is no network then check if offline content is enabled or not.
    if (networkState == 'none') {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
            $.ajax({
                url: filepath,
                type: 'get',
                dataType: 'text',
                success: function (data) {
                    // If offline content is disabled and there is no internet then show popup.
                    if (data !== 'on') {
                        $("#nointernet").popup("option", "overlayTheme", "a");
                        setTimeout(function () {
                            $("#nointernet").popup("open");
                        }, 100);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    /**
                     * If offline.txt is not there, that means it is deleted either or is not synced.
                     * display popup to user asking him to either sync the data or choose online version.
                     **/

                },
            });
        });
    }
}

$(document).on('vclick', '#okerror', function (e) {
    navigator.app.exitApp();
});

function exitFromApp() {
    navigator.app.exitApp();
}

function clearCache() {
    navigator.camera.cleanup();
}

function capturePhotoWithFile() {
    if (navigator.camera !== undefined) {
        navigator.camera.getPicture(onPhotoFileSuccess, onFail, {
            quality: 50,
            targetWidth: 960,
            targetHeight: 640,
            destinationType: Camera.DestinationType.FILE_URI
        });
    } else {
        isCamera = false;
    }
}

function onPhotoFileSuccess(imageData) {
    clearCache();
    $('#photoptions').show();
    $('#confirmArea').hide();
    // Get image handle
    var smallImage = document.getElementById('smallImage');
    // Unhide image elements
    smallImage.style.display = 'block';
    // Show the captured photo
    smallImage.src = imageData;
}

function onFail(message) {
    $('#photoptions').hide();
    $('#confirmArea').show();
    $("#confirmPop").popup("option", "overlayTheme", "a");
    setTimeout(function () {
        $("#confirmPop").popup("open");
    }, 100);
}

//Upload Image after capturing it
function uploadimage() {

    var srcimg = $('#smallImage').attr("src");
    var new_title = srcimg.substr(srcimg.lastIndexOf('/') + 1);

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.mimeType = "image/jpeg";

    var nid = localStorage.getItem('nid');

    options.fileName = new_title;

    var userObj = getUserObject();
    var uid = userObj.user.uid;

    var params = new Object();

    params.nid = nid;
    params.title = new_title;
    params.userid = uid;

    options.params = params;
    options.chunkedMode = true;

    var ft = new FileTransfer();
    ft.upload(srcimg, baseurl + "/upload.php", winupload, failupload, options);
    createloadingmsg('Uploading Photo');
}

function winupload(r) {
    if (r.responseCode == 200) {
        $('.ui-loader').css('display', 'none');
        createloadingmsg('Photo uploaded successfully');
        $.mobile.changePage("#travellerphotos");
    }
}

function failupload(error) {
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
    $.mobile.changePage("#travellerphotos");
}

//Code For Removal of Files from Cache , Cleaning process if user press Cancel Upload
function cancelUpload() {
    var srcimg = $('#smallImage').attr("src");
    createloadingmsg('Deleting Temporary Photo');
    window.resolveLocalFileSystemURI(srcimg, resok, resfail);
}

function resok(fileEntry) {
    fileEntry.remove(fdok, fdfail);
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
}

function resfail(error) {
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
    $.mobile.changePage("#travellerphotos");
}

function fdok() {
    $.mobile.hidePageLoadingMsg();
    $.mobile.changePage("#travellerphotos");
}

function fdfail(error) {
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
    $.mobile.changePage("#travellerphotos");
}
// END OF CLEANING


function getImage() {
    if ($.trim($('#title').val()) != '') {
        // Retrieve image file location from specified source
        navigator.camera.getPicture(uploadPhoto, function (message) {
            $('.ui-loader').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
        }, {
            quality: 50,
            destinationType: navigator.camera.DestinationType.FILE_URI,
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
            targetWidth: 960,
            targetHeight: 640
        });
        createloadingmsg('Uploading Photo');
    } else {
        alert("Please provide title");
    }
}

function uploadPhoto(imageURI) {
    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.mimeType = "image/jpeg";

    var nid = localStorage.getItem('nid');
    var title = $('#title').val();

    var new_title = title.split(' ').join('_')
    options.fileName = new_title;

    var userObj = getUserObject();
    var uid = userObj.user.uid;

    var params = new Object();
    params.nid = nid;
    params.title = new_title;
    params.userid = uid;

    options.params = params;
    options.chunkedMode = true;

    var ft = new FileTransfer();
    ft.upload(imageURI, baseurl + "/upload.php", win, fail, options);

}

function win(r) {
    var response = JSON.parse(r.response);
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
    $('#title').val('');
    $.mobile.changePage("#travellerphotos");
}

function fail(error) {
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
}

$.fn.loadNicely = function (options) {

    var defaults = {
        preLoad: function (img) {
        },
        onLoad: function (img) {
            $(img).fadeIn(200);
        }
    };

    var options = $.extend(defaults, options);

    return this.each(function () {

        if (!this.complete) {
            options.preLoad(this);
            $(this).load(function () {
                options.onLoad(this);
            }).attr("src", this.src);
        } else {
            options.onLoad(this);
        }
    });
};

function openBirthDate() {

    var now = new Date();
    var days = {};
    var years = {};
    var months = {
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dec'
    };

    for (var i = 1; i < 32; i += 1) {
        days[i] = i;
    }

    for (i = now.getFullYear() - 100; i < now.getFullYear() + 1; i += 1) {
        years[i] = i;
    }

    SpinningWheel.addSlot(years, 'right', 1999);
    SpinningWheel.addSlot(months, '', 4);
    SpinningWheel.addSlot(days, 'right', 12);

    SpinningWheel.setCancelAction(cancel);
    SpinningWheel.setDoneAction(done);

    SpinningWheel.open();
}

function done() {
    var results = SpinningWheel.getSelectedValues();
    var mydate = results.selDate;
    $('#dobtext').val(mydate);
}

function cancel() {
    document.getElementById('result').innerHTML = 'cancelled!';
}

function getAppDefaults() {
    $.ajax({
        url: baseurl + "/demo/demo",
        type: 'get',
        data: 'method=getDefaultConfigs',
        dataType: 'json',
        success: function (data) {
            localStorage.setItem('appDefaults', JSON.stringify(data));
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (XMLHttpRequest.status == 503) {
                $.mobile.changePage('#offlineapp');
            }
            localStorage.setItem('appDefaults', '');
            return false;
        },
    });
}

// Twitter Login Redirect
$(document).on('vclick', '#twid', function (e) {
    createloadingmsg('Redirecting To Twitter');
    loginTwitter();
});

// Facebook Login Redirect URL
$(document).on('vclick', '#fbid', function (e) {
    createloadingmsg('Redirecting To Facebook');
    login();
});

// Google Login URL
$(document).on('vclick', '#gid', function (e) {
    createloadingmsg('Redirecting To Google');
    googleapi.authorize({
        client_id: '375224829816-k0f40o6l1pi5kvv0o6j6uf3ltqj838vb.apps.googleusercontent.com',
        client_secret: 'h08Owkemi5NMFxDeRtcGWdBn',
        redirect_uri: 'https://www.mumbaicolors.com/oauth2callback',
        scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email'
    }).done(function (data) {
        localStorage.setItem('googletoken', data.access_token);
        googleapi.getUserInfo(data.access_token);
    }).fail(function (data) {

    });
});

$(document).on('click', '#toggle', function (e) {

    if (drawerStatus == false) {
        drawerStatus = true;
    } else {
        drawerStatus = false;
    }

    if (drawerStatus) {
        $(".ui-loader-background").css('display', 'block');
    } else {
        $(".ui-loader-background").css('display', 'none');
    }

});

$(document).on('vclick', '#signin', function (e) {

    var current_page = $.mobile.activePage.attr('id');

    var username = $('#' + current_page).find("#email").val();
    var password = $('#' + current_page).find("#pw").val();

    var flag = true;
    var invalid = false;

    if ($.trim(username) == '') {
        $('#' + current_page).find("#email").parent().addClass("yesmandatory");
        flag = false;
    } else {
        if (!isValidEmailAddress(username)) {
            $('#' + current_page).find("#email").parent().addClass("yesmandatory");
            invalid = true;
        } else {
            $('#' + current_page).find("#email").parent().removeClass("yesmandatory");
        }
    }

    if ($.trim(password) == '') {
        $('#' + current_page).find("#pw").parent().addClass("yesmandatory");
        flag = false;
    } else {
        $('#' + current_page).find("#pw").parent().removeClass("yesmandatory");
    }

    if (flag) {
        if (!invalid) {
            $('.ui-loader').css('display', 'block');

            // BEGIN: drupal services user login (warning: don't use https if you don't have ssl setup)
            $.ajax({
                url: baseurl + "/services/session/token",
                type: "get",
                dataType: "text",
                beforeSend: function () {
                },
                error: function (jqXHR, textStatus, errorThrown) {

                },
                success: function (token) {

                    $.ajax({
                        url: baseurl + "/demo/user/login.json",
                        type: 'post',
                        data: 'username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password),
                        dataType: 'json',
                        error: function (XMLHttpRequest, textStatus, errorThrown) {

                            if (XMLHttpRequest.status == '401') {
                                $('.invalid_userpass').show();
                                $('.invalid_userpass').html("Invalid Username/Password.");
                                $('.ui-loader').css('display', 'none');
                                $.mobile.hidePageLoadingMsg();
                                return false;
                            }

                            if (XMLHttpRequest.status == '406') {
                                $('.ui-loader').css('display', 'none');
                                $.mobile.hidePageLoadingMsg();
                                $.mobile.changePage("#homepage");
                            }

                        },
                        beforeSend: function (request) {
                            createloadingmsg('Authenticating');
                            request.setRequestHeader("X-CSRF-Token", token);
                        },
                        success: function (data) {
                            var userObj = data;
                            if (userObj !== null) {
                                var drupal_user = userObj.user;

                                if (drupal_user.uid == 0) { // user is not logged in, show the login button, hide the logout button
                                    $('#loginForm').removeClass('hide');
                                    $('#loginForm').addClass('show');
                                    $('#logoutBtn').removeClass('show');
                                    $('#logoutBtn').addClass('hide');
                                } else { // user is logged in, hide the login button, show the logout button
                                    $('#loginForm').removeClass('show');
                                    $('#loginForm').addClass('hide');
                                    $('#logoutBtn').removeClass('hide');
                                    $('#logoutBtn').addClass('show');

                                    $('.ui-loader').css('display', 'none');
                                    $.mobile.hidePageLoadingMsg();

                                    $.mobile.changePage('#homepage');

                                }
                            }
                            sessionStorage.userObj = JSON.stringify(data);
                        }
                    });
                }
            });
        } else {
            $('.invalid_userpass').show();
            $('.invalid_userpass').html("Invalid Email ID.");
        }
    } else {
        $('.invalid_userpass').show();
        $('.invalid_userpass').html("Email/Password cannot be blank.");
        return false;
    }
});

$(document).on('click', '#guest', function (event) {
    $.mobile.changePage('#homepage');
});

$(document).on('click', '.adfooter', function (event) {
    $.mobile.changePage('#loginpage');
});

$(document).on('pagebeforecreate', '#ads', function (event) {
    var no = Math.floor((Math.random() * 13) + 1);
    $('.backgroundimg').addClass("backgroundimg" + no);

    $('.adfooter').hide();
    $('#ads .adimage').hide();
    createloadingmsg();
    $.ajax({
        url: baseurl + "/demo/demo",
        type: 'get',
        data: 'method=getDefaultConfigs',
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $.mobile.changePage('#loginpage');
        },
        success: function (data) {
            $('.adfooter').show();
            var active = data.ads.active;
            if (active == '1') {
                var title = data.ads.title;
                var content = data.ads.content;
                var image = data.ads.image;
                if (image == '') {
                    $('#ads .adimage').hide();
                } else {
                    $('#ads .adimage').show();
                }
                $('#ads .adimage img').attr('src', image);
                $('#ads .adtitle').html(title);
                $('#ads .adcontent .txtcontent').html(content);
            } else {
                $.mobile.changePage('#loginpage');
            }
        },
        complete: function () {
            $('.ui-loader').css('display', 'none');
            $('.ui-loader-background').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
        }
    });
});

$(document).on('click', '#loginreg', function (event) {
    $.mobile.changePage('#registerPage');
});

$(document).on('pagebeforecreate', '#loginpage', function (event) {
    getAppDefaults();
    var appDefault = localStorage.getItem('appDefaults');
    if(appDefault !== ''){
        var configs = JSON.parse(appDefault);
        localStorage.setItem('weatherapiurl', configs.config.weatherapiurl);
        filesRoot = configs.config.filesroot;
        filesRoot = filesRoot.replace(/\/$/, '');
        baseurl = configs.config.baseurl;
    }

    $('.invalid_userpass').hide();
    var no = Math.floor((Math.random() * 13) + 1);
    $('.backgroundimg').addClass("backgroundimg" + no);
    $("#loginpage .blur").animate({
        opacity: 0.6
    }, 3000, function () {
        // Animation complete.
    });
});

$(document).on('pageshow', '#loginpage', function (event) {
    $('.ui-loader').css('display', 'none');
    $('.ui-loader-background').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
    $(document).on('vclick', '#registerbtn', function () {
        $.mobile.changePage('#registerPage');
        return false;
    });

    $("#loginpage").swipeleft(function () {
        $.mobile.changePage("#homepage", {
            transition: "slide",
            reverse: false,
            changeHash: false,
            reloadPage: false
        });
    });

    $("#loginpage").swiperight(function () {
        $.mobile.changePage("#homepage", {
            transition: "slide",
            reverse: true,
            changeHash: false,
            reloadPage: false
        });
    });

});

$(document).on('pageshow', '#offline', function (event) {
    //checkOfflineOrNot();
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        fileSystem.root.getFile('castaway/offline.txt', null, function (fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function (evt) {
                    var offlineStatus = evt.target.result;
                    if (offlineStatus == 'on') {
                        $('#flip-3').val('on').slider('refresh');
                        if (localStorage.getItem('networkState') == 'none') {
                            $('#available_version').html(localStorage.getItem('lastsystemversion'));
                        } else {
                            $.ajax({
                                url: baseurl + "/demo/demo",
                                type: 'get',
                                data: 'method=getAvailableVersion',
                                dataType: 'json',
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    $.mobile.changePage('#loginpage');
                                },
                                success: function (data) {
                                    var available_version = parseFloat(data.available_version);
                                    localStorage.setItem('lastsystemversion', available_version.toFixed(1));
                                    $('#available_version').html(available_version.toFixed(1));
                                }
                            });
                        }
                    } else {
                        $('#flip-3').val('off').slider('refresh');
                        $.ajax({
                            url: baseurl + "/demo/demo",
                            type: 'get',
                            data: 'method=getAvailableVersion',
                            dataType: 'json',
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $.mobile.changePage('#loginpage');
                            },
                            success: function (data) {
                                var available_version = parseFloat(data.available_version);
                                localStorage.setItem('lastsystemversion', available_version.toFixed(1));
                                $('#available_version').html(available_version.toFixed(1));
                            }
                        });
                    }
                };
                reader.readAsText(file);
            }, function () {
            });
        }, function () {
        });
    });

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "/castaway/data/versions/version.txt";
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                var current_version = parseFloat(data);
                $('#current_version').html(current_version.toFixed(1));
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });

    // No Offline.txt found then need to ask user to download or sync it.
    var networkState = navigator.network.connection.type;
    // If internet is not available then ask user to connect the internet to sync data.
    if( networkState == 'none') {
        $('#startDl').addClass('ui-disabled');
    }else{
        $('#startDl').removeClass('ui-disabled');
    }  
});

$(document).on('pagebeforecreate', '#forgotpass', function (event) {
    var no = Math.floor((Math.random() * 13) + 1);
    $('.backgroundimg').addClass("backgroundimg" + no);

    $("#forgotpass .blur").animate({
        opacity: 0.6
    }, 3000, function () {
        // Animation complete.
    });
});

$(document).on('pageshow', '#forgotpass', function (event) {

    $(document).on('vclick', '#forgotbtn', function () {

        var username = $('#forgotpass').find("#un").val();

        var flag = true;

        if ($.trim(username) == '') {
            $('#forgotpass').find("#un").parent().addClass("yesmandatory");
            flag = false;
        } else {
            $('#forgotpass').find("#un").parent().removeClass("yesmandatory");
        }

        if (flag) {
            createloadingmsg('Resetting Password');
            $.ajax({
                url: baseurl + "/services/session/token",
                type: "get",
                dataType: "text",
                error: function (jqXHR, textStatus, errorThrown) {

                },
                success: function (token) {
                    var forgotFlag = 0;
                    $.ajax({
                        url: baseurl + "/demo/demo",
                        type: 'post',
                        data: 'method=forgotPass&username=' + encodeURIComponent(username),
                        dataType: 'json',
                        error: function (XMLHttpRequest, textStatus, errorThrown) {

                        },
                        beforeSend: function (request) {
                            request.setRequestHeader("X-CSRF-Token", token);
                        },
                        success: function (data) {
                            if (data.status == 1) {
                                forgotFlag = 1;
                                $('#forgotErrorMessage').html(data.error);
                                $('#forgotError').popup("open");

                            } else {
                            }
                        },
                        complete: function () {
                            $('.ui-loader').css('display', 'none');
                            $.mobile.hidePageLoadingMsg();
                        }
                    });
                }
            });
            return false;
        } else {
            return false;
        }
    });
});

$(document).on('vclick', '#dob', function () {
    openBirthDate();
});

$(document).on('pagebeforecreate', '#registerPage', function (event) {
    var no = Math.floor((Math.random() * 13) + 1);
    $('.backgroundimg').addClass("backgroundimg" + no);

    $("#registerPage .blur").animate({
        opacity: 0.6
    }, 3000, function () {
        // Animation complete.
    });
});

$(document).on('pageshow', '#registerPage', function (event) {

    $(document).on('vclick', '#regsubmit', function () {

        var email = $('#registerScreen').find("#email").val();
        var password = $('#registerScreen').find("#pw").val();

        var flag = true;
        var invalid = false;

        if ($.trim(password) == '') {
            $('#registerScreen').find("#pw").parent().addClass("yesmandatory");
            flag = false;
        } else {
            $('#registerScreen').find("#pw").parent().removeClass("yesmandatory");
        }

        if ($.trim(email) == '') {
            $('#registerScreen').find("#email").parent().addClass("yesmandatory");
            flag = false;
        } else {
            if (!isValidEmailAddress(email)) {
                $('#registerScreen').find("#email").parent().addClass("yesmandatory");
                invalid = true;
            } else {
                $('#registerScreen').find("#email").parent().removeClass("yesmandatory");
            }
        }

        if (flag) {
            if (!invalid) {
                $('.invalid_userpass').hide();
                createloadingmsg('Registering User');
                $.ajax({
                    url: baseurl + "/services/session/token",
                    type: "get",
                    dataType: "text",
                    error: function (jqXHR, textStatus, errorThrown) {

                    },
                    success: function (token) {
                        var registered = 0;
                        $.ajax({
                            url: baseurl + "/demo/demo",
                            type: 'post',
                            data: 'method=registerUser&email=' + email + '&password=' + encodeURIComponent(password),
                            dataType: 'json',
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                            },
                            beforeSend: function (request) {
                                request.setRequestHeader("X-CSRF-Token", token);
                            },
                            success: function (data) {
                                if (data.uid !== 0) {
                                    registered = 1;
                                } else {
                                    $('#registerErrorMessage').html(data.error);
                                    $('#registerError').popup("open");
                                }
                            },
                            complete: function () {
                                $('.ui-loader').css('display', 'none');
                                $.mobile.hidePageLoadingMsg();
                                if (registered == 1) {
                                    $('#registerPopup').popup("open");
                                }
                            }
                        });
                    }
                });
            } else {
                $('.invalid_userpass').show();
                $('.invalid_userpass').html("Invalid Email ID.");
            }
        } else {
            $('.invalid_userpass').show();
            $('.invalid_userpass').html("Email/Password cannot be blank.");
            return false;
        }

        return false;

    });

    $(document).on('vclick', '#registerLogin', function () {
        $.mobile.changePage("#loginpage");
    });
});

function redirectIfLoggedIn() {

    //Checking if user is logged in or not
    try {
        // Obtain session token.
        $.ajax({
            url: baseurl + "/services/session/token",
            type: "get",
            dataType: "text",
            error: function (jqXHR, textStatus, errorThrown) {

            },
            success: function (token) {
                // Call system connect with session token.
                $.ajax({
                    url: baseurl + "/demo/system/connect.json",
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-Token", token);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    success: function (data) {
                        var drupal_user = data.user;
                        if (drupal_user.uid == 0) { // user is not logged in, show the login button, hide the logout button

                        } else { // user is logged in, hide the login button, show the logout button
                            $.mobile.changePage("#homepage");
                        }
                    }
                });
            }
        });
    } catch (error) {
    }
}

function checkUserLoggedIn() {
    //Checking if user is logged in or not
    try {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
            $.ajax({
                url: filepath,
                type: 'get',
                dataType: 'text',
                success: function (data) {
                    if (data == 'on') {
                        checkOfflineUserLoggedIn(fileSystem.root.toURL());
                    } else {
                        checkOnlineUserLoggedIn();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                },
            });
        });
    } catch (error) {
    }
}

function checkOfflineUserLoggedIn() {
    var guestobj = {};
    var guestobjuserobj = {};
    sessionStorage.tempuid = 0;
    guestobj.uid = 0;
    guestobj.email = '';
    guestobj.name = 'Guest';
    guestobjuserobj.user = guestobj;
    guestobjuserobj.profile = '';
    sessionStorage.userObj = JSON.stringify(guestobjuserobj);

    localStorage.setItem('loggedInUser', 'Guest');

    $('#loginForm').removeClass('show');
    $('#loginForm').addClass('hide');
    $('#logoutBtn').removeClass('hide');
    $('#logoutBtn').addClass('show');
    $("#loginlink").hide
    $("#rateit").show();
    $.mobile.changePage("#homepage");
}

function checkOnlineUserLoggedIn() {
    // Obtain session token.
    $.ajax({
        url: baseurl + "/services/session/token",
        type: "get",
        dataType: "text",
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (token) {
            // Call system connect with session token.
            $.ajax({
                url: baseurl + "/demo/system/connect.json",
                type: 'post',
                dataType: 'json',
                beforeSend: function (request) {
                    request.setRequestHeader("X-CSRF-Token", token);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                success: function (data) {
                    var drupal_user = data.user;

                    var twitterObj = localStorage.getItem('twitterdetails');
                    if (twitterObj !== null) {
                        var twitterobj = {};
                        var twitteruserobj = {};
                        var twitterData = JSON.parse(twitterObj);
                        sessionStorage.tempuid = twitterData.id;
                        twitterobj.uid = twitterData.id;
                        twitterobj.email = twitterData.name;
                        twitterobj.name = twitterData.name;
                        twitteruserobj.user = twitterobj;
                        twitteruserobj.profile = '';
                        sessionStorage.userObj = JSON.stringify(twitteruserobj);
                        var drupal_user = data.user;
                        isTwitterLogin = true;
                    }

                    var fbObj = localStorage.getItem('fbdetails');
                    if (fbObj !== null) {
                        var fbobj = {};
                        var fbuserobj = {};
                        var fbData = JSON.parse(fbObj);

                        $.each(fbData, function (key, value) {

                            if (key == 'email') {
                                sessionStorage.tempuid = value;
                                if (fbobj.uid == undefined) {
                                    get_uid_by_name(value);
                                }
                                fbobj.uid = sessionStorage.tempuid;
                                fbobj.email = value;
                            }
                            if (key == 'name') {
                                fbobj.name = value;
                            }

                        });
                        fbuserobj.user = fbobj;
                        fbuserobj.profile = '';
                        sessionStorage.userObj = JSON.stringify(fbuserobj);
                        var drupal_user = data.user;
                        isFbLogin = true;
                    }

                    var googleObj = localStorage.getItem('googledetails');
                    if (googleObj !== null) {
                        var googleobj = {};
                        var googleuserobj = {};
                        var googleData = JSON.parse(googleObj);
                        sessionStorage.tempuid = googleData.id;
                        googleobj.uid = sessionStorage.tempuid;
                        googleobj.email = googleData.email;
                        googleobj.name = googleData.name;
                        googleuserobj.user = googleobj;
                        googleuserobj.profile = '';
                        sessionStorage.userObj = JSON.stringify(googleuserobj);
                        var drupal_user = data.user;
                        isGoogleLogin = true;
                    }
                    localStorage.setItem('userObj', JSON.stringify(drupal_user));

                    // If user is not logged in with APP username/password, instead logged in with social networking or guest.
                    if (drupal_user.uid == 0) {
                        $('#loginForm').removeClass('hide');
                        $('#loginForm').addClass('show');
                        $('#logoutBtn').removeClass('show');
                        $('#logoutBtn').addClass('hide');
                        $("#loginlink").show();

                        $('.userinfo').hide();
                        $("#rateit").hide();

                        if (isFbLogin) {
                            $('#loginForm').removeClass('show');
                            $('#loginForm').addClass('hide');
                            $('#logoutBtn').removeClass('hide');
                            $('#logoutBtn').addClass('show');
                            $("#loginlink").hide();
                            localStorage.setItem('loggedInUser', fbData.name);
                            $('#userdetail').html("Hello, " + fbData.name);
                            $('.userinfo').show();
                            $("#rateit").show();
                            $.mobile.changePage("#homepage");
                        } else if (isGoogleLogin) {
                            $('#loginForm').removeClass('show');
                            $('#loginForm').addClass('hide');
                            $('#logoutBtn').removeClass('hide');
                            $('#logoutBtn').addClass('show');
                            $("#loginlink").hide();
                            localStorage.setItem('loggedInUser', googleData.name);
                            $('#userdetail').html("Hello, " + googleData.name);
                            $('.userinfo').show();
                            $("#rateit").show();
                            $.mobile.changePage("#homepage");
                        } else if (isTwitterLogin) {
                            $('#loginForm').removeClass('show');
                            $('#loginForm').addClass('hide');
                            $('#logoutBtn').removeClass('hide');
                            $('#logoutBtn').addClass('show');
                            $("#loginlink").hide();
                            localStorage.setItem('loggedInUser', twitterData.screen_name);
                            $('#userdetail').html("Hello, " + twitterData.screen_name);
                            $('.userinfo').show();
                            $("#rateit").show();
                        } else {
                            $('#loginForm').removeClass('show');
                            $('#loginForm').addClass('hide');
                            $('#logoutBtn').removeClass('hide');
                            $('#logoutBtn').addClass('show');
                            $("#loginlink").hide();
                            localStorage.setItem('loggedInUser', 'Guest');
                            $('.userinfo').show();
                            $("#rateit").show();
                            sessionStorage.userObj = JSON.stringify(data);
                        }
                    } else { // user is logged in, hide the login button, show the logout button
                        if (data.user.name !== undefined) {
                            var name = data.user.name;
                        } else {
                            var name = data.profile.main.field_name.und[0].value;
                        }
                        localStorage.setItem('loggedInUser', name);
                        loggedInUser = name;
                        if (name == undefined) {
                            var name = 'Guest';
                            loggedInUser = 'Guest';
                        }
                        localStorage.setItem('loggedInUser', name);

                        $('#loginForm').removeClass('show');
                        $('#loginForm').addClass('hide');
                        $('#logoutBtn').removeClass('hide');
                        $('#logoutBtn').addClass('show');
                        $("#loginlink").hide
                        $("#rateit").show();
                        sessionStorage.userObj = JSON.stringify(data);
                        $.mobile.changePage("#homepage");
                    }
                }
            });
        }
    });
}

function get_uid_by_name(name) {

    $.ajax({
        url: baseurl + "/services/session/token",
        type: "get",
        dataType: "text",
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (token) {
            $.ajax({
                url: baseurl + "/demo/demo",
                type: 'post',
                async: false,
                data: 'method=getUserByName&name=' + name,
                dataType: 'json',
                beforeSend: function (request) {
                    request.setRequestHeader("X-CSRF-Token", token);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                success: function (data) {
                    if (data.uid !== '' || data.uid !== undefined) {
                        fbuserobj.user.uid = data.uid;
                        googleuserobj.user.uid = data.uid;
                        sessionStorage.userObj = JSON.stringify(fbuserobj);
                    }
                },
                complete: function () {
                }
            });
        }
    });
}

function registerFbUser(userobj) {
    var isFbRegistered = localStorage.getItem('isFbRegistered');
    if (!isFbRegistered) {
        $.ajax({
            url: baseurl + "/services/session/token",
            type: "get",
            dataType: "text",
            error: function (jqXHR, textStatus, errorThrown) {
            },
            success: function (token) {
                $.ajax({
                    url: baseurl + "/demo/demo",
                    type: 'post',
                    data: 'method=registerFbUser&userdata=' + userobj,
                    dataType: 'json',
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-Token", token);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    success: function (data) {
                        localStorage.setItem('isFbRegistered', true);
                    },
                    complete: function () {
                    }
                });
            }
        });
    }
}

function registerGoogleUser(userobj) {
    var isGoogleRegistered = localStorage.getItem('isGoogleRegistered');
    if (!isGoogleRegistered) {
        $.ajax({
            url: baseurl + "/services/session/token",
            type: "get",
            dataType: "text",
            error: function (jqXHR, textStatus, errorThrown) {
            },
            success: function (token) {
                $.ajax({
                    url: baseurl + "/demo/demo",
                    type: 'post',
                    data: 'method=registerGoogleUser&googledata=' + userobj,
                    dataType: 'json',
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-Token", token);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    success: function (data) {
                        localStorage.setItem('isGoogleRegistered', true);
                    },
                    complete: function () {
                    }
                });
            }
        });
    }
}

function letmelogout() {
    createloadingmsg('Logging Out');
    if (isFbLogin) {
        logout();
        sessionStorage.clear();
        localStorage.clear();
        isFbLogin = false;
        isGoogleLogin = false;
        isTwitterLogin = false;
        $.mobile.changePage("#loginpage");

    } else if (isGoogleLogin) {
        logoutGoogle();
        sessionStorage.clear();
        localStorage.clear();
        isFbLogin = false;
        isGoogleLogin = false;
        isTwitterLogin = false;
        $.mobile.changePage("#loginpage");

    } else if (isTwitterLogin) {
        logoutTwitter();
        sessionStorage.clear();
        localStorage.clear();
        isFbLogin = false;
        isGoogleLogin = false;
        isTwitterLogin = false;
        $.mobile.changePage("#loginpage");
    } else {
        var isGuest = getUserObject();

        if (isGuest.user.uid == 0 || isGuest == false) {
            sessionStorage.clear();
            localStorage.clear();
            $.mobile.changePage("#loginpage");
        } else {
            $.ajax({
                url: baseurl + "/services/session/token",
                type: "get",
                dataType: "text",
                error: function (jqXHR, textStatus, errorThrown) {
                },
                success: function (token) {
                    $.ajax({
                        url: baseurl + "/demo/user/logout.json",
                        type: 'post',
                        dataType: 'json',
                        beforeSend: function (request) {
                            $('.ui-loader').css('display', 'block');
                            request.setRequestHeader("X-CSRF-Token", token);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            if (XMLHttpRequest.status == '406') {
                                $('.ui-loader').css('display', 'none');
                                $.mobile.hidePageLoadingMsg();
                                $.mobile.changePage("#loginpage");
                            }
                        },
                        success: function (data) {
                            $('.ui-loader').css('display', 'none');
                            $.mobile.hidePageLoadingMsg();
                            $('#userdetail').html('');
                            localStorage.removeItem("userObj");
                            sessionStorage.clear();
                            if (localStorage.getItem('fbdetails') !== '') {
                                logout();
                            }
                            if (localStorage.getItem('googledetails') !== '') {
                                logoutGoogle();
                            }
                            localStorage.clear();
                            $.mobile.changePage("#loginpage");
                        }
                    });
                }
            });
        }
    }
}

function logoutGoogle() {
    var googletoken = localStorage.getItem('googletoken');
    $.post("https://accounts.google.com/o/oauth2/revoke?token=" + googletoken, function () {
        createloadingmsg('Logging Out From Google');
    })
        .done(function (data) {
            localStorage.removeItem('googledetails');
            localStorage.removeItem('googletoken');
            localStorage.setItem('fbConnected', false);
            $('.ui-loader').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
        })
        .fail(function (response) {
        });
}

function logoutTwitter() {
    localStorage.removeItem('twitterdetails');
    localStorage.removeItem('twittertoken');
    localStorage.setItem('twitterConnected', false);
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
}

function getUserObject() {
    if (sessionStorage.userObj !== undefined) {
        return JSON.parse(sessionStorage.userObj);
    } else {
        return false;
    }
}

$(document).click(function () {
    //$("#dl-menu ul.dl-menu").removeClass( 'dl-menuopen' );
    //$("#dl-menu button").removeClass( 'dl-active' );
});

$(document).on('pagebeforecreate', '#homepage', function (event) {

    //$('#homepage #dl-menu').dlmenu();

    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });

    if (device == 'mobile') {
        $('#exitbtn').show();
    } else {
        $('#exitbtn').hide();
    }
});

$(document).on('vclick', '#loginForm', function () {
    $("#un").attr('value', '');
    $("#pw").attr('value', '');
    $("#un").parent().removeClass("yesmandatory");
    $("#pw").parent().removeClass("yesmandatory");
});

$(document).on('pagebeforeshow', '#homepage', function (event) {
    $("#title_home").html("Castaway");
    checkUserLoggedIn();
});

/** Dashboard Of The App , This will fetch the categories dynamically from web service when page is created **/
$(document).on('pageshow', '#homepage', function (event) {

    var loggedInUser = localStorage.getItem('loggedInUser');

    if (loggedInUser == null) {
        $('#userdetail').html("Hello, Guest");
        $('.userinfo').show();
    } else {
        $('#userdetail').html("Hello, " + loggedInUser);
        $('.userinfo').show();
    }
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();

    var userObj = getUserObject();
    initializeGpsLocation();

    if (userObj !== false) {
        if (userObj.user.profile == undefined) {
            var name = userObj.user.name;
        } else {
            var name = userObj.profile.main.field_name.und[0].value;
        }
        $('#logoutBtn').removeClass('hide');
        $('#logoutBtn').addClass('show');
        $('.userinfo').show();
    } else {

        //  $.mobile.changePage("#loginpage");
    }
    $("#logoutBtn").show();

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        localStorage.setItem('sdcardpath', fileSystem.root.toURL());
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {
                    createOfflineCategories(fileSystem.root.toURL());
                } else {
                    createCategories();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });
    //$("#dl-menu ul.dl-menu").removeClass( 'dl-menuopen' );
});

function createOfflineCategories(filepath) {

    $.ajax({
        url: filepath + 'castaway/data/cache/all_categories.json',
        type: 'get',
        dataType: 'json',
        success: function (data) {

            var jsonstr = JSON.stringify(data);
            var regex = new RegExp(filesRoot, "g");
            var url_Replaced = jsonstr.replace(regex, filepath + 'castaway/data/');
            var data = JSON.parse(url_Replaced);
            var html = '';
            var view_height = $(window).height();
            var overviewImgHeight = view_height / 2;
            $('#categoriesList').addClass('emptycontainer');
            $('#categoriesList').css('height', overviewImgHeight);
            $('#categoriesList').addClass('is-loading');

            $.each(data, function (key, value) {
                html += '<li class="noshadow"><a id="' + value.tid + '" href="#"><img style="height: 100%;" src="' + value.imagepath + '"><h2>' + value.title + '</h2></a></li>';
                if (value.tid !== '') {
                    localStorage.setItem('tip-' + value.tid, value.tips);
                }
            });

            $('#categoriesList').html(html);
            $('#categoriesList').listview("refresh");

            $('#categoriesList').removeClass('emptycontainer');
            $('#categoriesList').removeClass('is-loading');

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        },
    });
}

function createCategories() {
    //Calculation of height of overview image and show loading while loading.
    var view_height = $(window).height();
    var overviewImgHeight = view_height / 2;
    $('#categoriesList').addClass('emptycontainer');
    $('#categoriesList').css('height', overviewImgHeight);
    $('#categoriesList').addClass('is-loading');

    var html = '';
    var data = '';
    var cached = localStorage.getItem('stationsCategories');
    if (cached === null) {
        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=getCategories',
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.mobile.changePage("#homepage");
            },
            success: function (data) {
                var data = JSON.stringify(data);
                localStorage.setItem('stationsCategories', data);
                var parseddata = $.parseJSON(data);
                $.each(parseddata, function (key, value) {
                    html += '<li class="noshadow"><a id="' + value.tid + '" href="#"><img style="height: 100%;" src="' + value.imagepath + '"><h2>' + value.title + '</h2></a></li>';
                    if (value.tid !== '') {
                        localStorage.setItem('tip-' + value.tid, value.tips);
                    }
                });
                $('#categoriesList').html(html);
                $('#categoriesList').listview("refresh");
            },
            complete: function () {
                $('#categoriesList').removeClass('emptycontainer');
                $('#categoriesList').removeClass('is-loading');
            }
        });
    } else {
        var parseddata = $.parseJSON(cached);
        $.each(parseddata, function (key, value) {
            html += '<li class="noshadow"><a id="' + value.tid + '" href="#"><img style="height: 100%;" src="' + value.imagepath + '"><h2>' + value.title + '</h2></a></li>';
            if (value.tid !== '') {
                localStorage.setItem('tip-' + value.tid, value.tips);
            }
        });
        $('#categoriesList').html(html);
        $('#categoriesList').listview("refresh");

        $('#categoriesList').removeClass('emptycontainer');
        $('#categoriesList').removeClass('is-loading');
    }
}

function createAttractionsList() {
    //Calculation of height of overview image and show loading while loading.
    var view_height = $(window).height();
    var overviewImgHeight = view_height;
    $('#attractionsList').html('');
    $('#attractionsList').css('height', overviewImgHeight);
    $('#attractionsList').addClass('is-loading');

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        localStorage.setItem('sdcardpath', fileSystem.root.toURL());
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {
                    getOfflineAttractionsList(fileSystem.root.toURL());
                } else {
                    getAttractionsList();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });

}

function getOfflineAttractionsList(filepath) {
    var html = '';
    var data = '';
    var cached = localStorage.getItem('stationsCategories');
    if (cached === null) {
        $.ajax({
            url: filepath + 'castaway/data/cache/all_plan_by_attractions_listing.json',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                var data = JSON.stringify(data);
                var regex = new RegExp(filesRoot, "g");
                var url_Replaced = data.replace(regex, fileSystem.root.toURL() + 'castaway/data/');
                var parseddata = $.parseJSON(url_Replaced);
                localStorage.setItem('attractionsCategories', data);
                var parseddata = $.parseJSON(data);
                $.each(parseddata, function (key, value) {
                    html += '<li class="' + value.class + '" id="' + value.tid + '"><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span>' + value.description + '</span></div></li>';
                    if (value.tid !== '') {
                        localStorage.setItem('tip-' + value.tid, value.tips);
                    }
                });
                $('#attractionsList').html(html);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
            complete: function () {
                $('#attractionsList').removeClass('emptycontainer');
                $('#attractionsList').removeClass('is-loading');
            }
        });
    } else {
        var parseddata = $.parseJSON(cached);
        $.each(parseddata, function (key, value) {
            html += '<li class="' + value.class + '" id="' + value.tid + '"><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span>' + value.description + '</span></div></li>';
        });
        $('#attractionsList').html(html);
        $('#attractionsList').removeClass('emptycontainer');
        $('#attractionsList').removeClass('is-loading');
    }
}

function getAttractionsList() {
    var html = '';
    var data = '';
    var cached = localStorage.getItem('attractionsCategories');
    if (cached === null) {
        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=getPlanByAttractionsList',
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.mobile.changePage("#homepage");
            },
            success: function (data) {
                var data = JSON.stringify(data);
                localStorage.setItem('attractionsCategories', data);
                var parseddata = $.parseJSON(data);
                $.each(parseddata, function (key, value) {
                    html += '<li class="' + value.class + '" id="' + value.tid + '"><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span>' + value.description + '</span></div></li>';
                    if (value.tid !== '') {
                        localStorage.setItem('tip-' + value.tid, value.tips);
                    }
                });
                $('#attractionsList').html(html);
                //$('#attractionsList').listview("refresh");
            },
            complete: function () {
                $('#attractionsList').removeClass('emptycontainer');
                $('#attractionsList').removeClass('is-loading');
            }
        });
    } else {
        var parseddata = $.parseJSON(cached);
        $.each(parseddata, function (key, value) {
            html += '<li class="' + value.class + '" id="' + value.tid + '"><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span>' + value.description + '</span></div></li>';
        });
        $('#attractionsList').html(html);
        //$('#attractionsList').listview("refresh");

        $('#attractionsList').removeClass('emptycontainer');
        $('#attractionsList').removeClass('is-loading');
    }
}


function createMonthsList() {
    //Calculation of height of overview image and show loading while loading.
    var view_height = $(window).height();
    var overviewImgHeight = view_height;
    $('#monthsListing').html('');
    $('#monthsListing').css('height', overviewImgHeight);
    $('#monthsListing').addClass('is-loading');

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {
                    localStorage.setItem('offlineStatus', data);
                    getOfflineMonthsList(fileSystem.root.toURL());
                } else {
                    localStorage.setItem('offlineStatus', data)
                    getMonthsList();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });
}

function getOfflineMonthsList(filepath) {
    var html = '';
    var data = '';
    var cached = localStorage.getItem('getPlanByMonthList');
    if (cached === null) {
        $.ajax({
            url: filepath + 'castaway/data/cache/all_plan_by_months_listing.json',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                var data = JSON.stringify(data);
                localStorage.setItem('getPlanByMonthList', data);
                var parseddata = $.parseJSON(data);
                $.each(parseddata, function (key, value) {
                    html += '<li class="' + value.class + ' calicon" id=' + value.tid + '><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span>' + value.description + '</span></div></li>';
                    if (value.tid !== '') {
                        localStorage.setItem('tip-' + value.tid, value.tips);
                    }
                });
                $('#monthsListing').html(html);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
            complete: function () {
                $('#monthsListing').removeClass('emptycontainer');
                $('#monthsListing').removeClass('is-loading');
            }
        });
    } else {
        var parseddata = $.parseJSON(cached);
        $.each(parseddata, function (key, value) {
            html += '<li class="' + value.class + ' calicon" id=' + value.tid + '><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span>' + value.description + '</span></div></li>';
        });
        $('#monthsListing').html(html);
        $('#monthsListing').removeClass('emptycontainer');
        $('#monthsListing').removeClass('is-loading');
    }
}

function getMonthsList() {
    var html = '';
    var data = '';
    var cached = localStorage.getItem('getPlanByMonthList');
    if (cached === null) {
        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=getPlanByMonthList',
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.mobile.changePage("#homepage");
            },
            success: function (data) {
                var data = JSON.stringify(data);
                localStorage.setItem('getPlanByMonthList', data);
                var parseddata = $.parseJSON(data);
                $.each(parseddata, function (key, value) {
                    html += '<li class="' + value.class + ' calicon" id=' + value.tid + '><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span>' + value.description + '</span></div></li>';
                    if (value.tid !== '') {
                        localStorage.setItem('tip-' + value.tid, value.tips);
                    }
                });
                $('#monthsListing').html(html);
            },
            complete: function () {
                $('#monthsListing').removeClass('emptycontainer');
                $('#monthsListing').removeClass('is-loading');
            }
        });
    } else {
        var parseddata = $.parseJSON(cached);
        $.each(parseddata, function (key, value) {
            html += '<li class="' + value.class + ' calicon" id=' + value.tid + '><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span>' + value.description + '</span></div></li>';
        });
        $('#monthsListing').html(html);
        $('#monthsListing').removeClass('emptycontainer');
        $('#monthsListing').removeClass('is-loading');
    }
}

function createRegionsList() {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {
                    localStorage.setItem('offlineStatus', data);
                    getOfflineRegionsList(fileSystem.root.toURL());
                } else {
                    localStorage.setItem('offlineStatus', data)
                    getRegionsList();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });
}

function getOfflineRegionsList(filepath) {
    //Calculation of height of overview image and show loading while loading.
    var view_height = $(window).height();
    var overviewImgHeight = view_height;
    $('#religionsList').html('');
    $('#religionsList').css('height', overviewImgHeight);
    $('#religionsList').addClass('is-loading');

    var html = '';
    var data = '';

    $.ajax({
        url: filepath + 'castaway/data/cache/all_plan_by_regions_listing.json',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var data = JSON.stringify(data);
            localStorage.setItem('getPlanByRegionsList', data);
            var parseddata = $.parseJSON(data);
            $.each(parseddata, function (key, value) {
                html += '<li class="' + value.class + '" id="' + value.tid + '"><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span class="quotes">' + value.description + '</span></div></li>';
                if (value.tid !== '') {
                    localStorage.setItem('tip-' + value.tid, value.tips);
                }
            });
            $('#religionsList').html(html);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        },
        complete: function () {
            $('#religionsList').removeClass('emptycontainer');
            $('#religionsList').removeClass('is-loading');
        }
    });
}

function getRegionsList() {
    //Calculation of height of overview image and show loading while loading.
    var view_height = $(window).height();
    var overviewImgHeight = view_height;
    $('#religionsList').html('');
    $('#religionsList').css('height', overviewImgHeight);
    $('#religionsList').addClass('is-loading');

    var html = '';
    var data = '';
    var cached = localStorage.getItem('getPlanByRegionsList');
    if (cached === null) {
        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=getPlanByRegionsList',
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.mobile.changePage("#homepage");
            },
            success: function (data) {
                var data = JSON.stringify(data);
                localStorage.setItem('getPlanByRegionsList', data);
                var parseddata = $.parseJSON(data);
                $.each(parseddata, function (key, value) {
                    html += '<li class="' + value.class + '" id="' + value.tid + '"><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span class="quotes">' + value.description + '</span></div></li>';
                    if (value.tid !== '') {
                        localStorage.setItem('tip-' + value.tid, value.tips);
                    }
                });
                $('#religionsList').html(html);
            },
            complete: function () {
                $('#religionsList').removeClass('emptycontainer');
                $('#religionsList').removeClass('is-loading');
            }
        });
    } else {
        var parseddata = $.parseJSON(cached);
        $.each(parseddata, function (key, value) {
            html += '<li class="' + value.class + '" id="' + value.tid + '"><div><span class="icon"></span><span class="box"></span><h3>' + value.title + '</h3><span class="quotes">' + value.description + '</span></div></li>';
        });
        $('#religionsList').html(html);
        $('#religionsList').removeClass('emptycontainer');
        $('#religionsList').removeClass('is-loading');
    }
}

/** Action Event when user taps on Category , it should be send to detail page enlisting list of stations **/
$(document).on('vclick', '#categoriesList li', function (event) {
    var id = $(this).find('a').attr('id');
    var category = $(this).find('a').text();
    localStorage.setItem('category', category);
    localStorage.setItem('id', id);
    //$('.ui-loader').css('display', 'block');

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {
                    localStorage.setItem('offlineStatus', data);
                    getOfflineStationsForCategories(fileSystem.root.toURL(), id);
                } else {
                    localStorage.setItem('offlineStatus', data)
                    getStationsForCategories(id);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });
});

function getOfflineStationsForCategories(filepath, id) {
    $.ajax({
        url: filepath + 'castaway/data/cache/all_stations.json',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            $('.ui-loader').css('display', 'block');
            var id = localStorage.getItem('id');
            var message = createloading(id);
            $.mobile.loading('show', {
                text: "",
                textVisible: true,
                theme: "a",
                textonly: true,
                html: message
            });
            var jsonstr = JSON.stringify(data);
            var data = JSON.parse(jsonstr);
            var filteredData = data[id];
            var jsonData = JSON.stringify(filteredData);
            localStorage.removeItem('stationsFiltered');
            localStorage.setItem('stationsFiltered', jsonData);
            $('.ui-loader').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
            $.mobile.changePage("#stations");
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        },
    });
}

function getStationsForCategories(id) {
    if (id != '') {
        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=getStationList&sid=' + parseInt(id),
            dataType: 'json',
            beforeSend: function (request) {
                $('.ui-loader').css('display', 'block');
                var id = localStorage.getItem('id');
                var message = createloading(id);
                $.mobile.loading('show', {
                    text: "",
                    textVisible: true,
                    theme: "a",
                    textonly: true,
                    html: message
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.mobile.changePage("#homepage");
            },
            success: function (data) {
                var data = JSON.stringify(data);
                localStorage.removeItem('stationsFiltered');
                localStorage.setItem('stationsFiltered', data);
            },
            complete: function () {
                $('.ui-loader').css('display', 'none');
                $.mobile.hidePageLoadingMsg();
                $.mobile.changePage("#stations");
            }
        });
    }
}

function searchStations() {
    var searchstr = $('#searchplc').val();
    if ($.trim(searchstr) !== '') {
        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=searchStations&searchstr=' + encodeURIComponent(searchstr),
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            },
            beforeSend: function (request) {
                createloadingmsg('Searching');
            },
            success: function (data) {
                var data = JSON.stringify(data);
                localStorage.setItem('SearchedStationsFiltered', data);
            },
            complete: function () {
                $('.ui-loader').css('display', 'none');
                $.mobile.hidePageLoadingMsg();
                $.mobile.changePage("#searchstations");
            }
        });
    } else {
        var errStr = "Please enter place to search.";
        $('#errmsg').html(errStr);
        $("#errorPop").popup("option", "overlayTheme", "a");
        $("#errorPop").popup("open");
        return false;
    }
    return false;
}

$(document).on('keypress', '#searchplc', function (event) {
    var key = event.which;
    if (key == 13) // the enter key code
    {
        var stations = searchStations();
        return stations;
    }
});

$(document).on('vclick', '#searchbtn', function (event) {
    var stations = searchStations();
    return stations;
});

$(document).on('pagebeforeshow', '#searchstations', function (event) {
    $(".searchlist").empty();
});

/** This will fetch the stations searched **/
$(document).on('pageshow', '#searchstations', function (event) {
    var data = JSON.parse(localStorage.getItem('SearchedStationsFiltered'));
    if (data.length > 0) {

        $("#searchStationsList").html("");

        var data = localStorage.getItem('SearchedStationsFiltered');

        var rating;
        var cards_data = [];
        var cardsObj = {};
        var overview = '';

        $.each($.parseJSON(data), function (key, value) {

            rating = value.rating / 20;
            overview = value.overview;
            if (value.nid !== null) {
                cardsObj = {
                    "stationName": value.station,
                    "stationImg": value.station_image,
                    "nid": value.nid,
                    "overview_short": $.trim(overview).substring(0, 100) + "....",
                    "rating": rating
                };
                cards_data.push(cardsObj);
            }
        });

        var content, columns, compiledCardTemplate = undefined;
        var MIN_COL_WIDTH = 300;
        content = $(".searchlist");

        compiledCardTemplate = Mustache.compile($("#card-template").html());

        //resize event handler
        function onResize() {
            var targetColumns = Math.floor($(document).width() / MIN_COL_WIDTH);
            if (columns != targetColumns) {
                layoutColumns();
            }
        }

        function layoutColumns() {

            content.detach();
            content.empty();

            columns = Math.floor($(document).width() / MIN_COL_WIDTH);

            var columns_dom = [];

            for (var x = 0; x < columns; x++) {
                var col = $('<div class="column">');
                col.css("width", Math.floor(100 / columns) + "%");
                columns_dom.push(col);

                content.append(col);
            }

            for (var x = 0; x < cards_data.length; x++) {
                var html = compiledCardTemplate(cards_data[x]);
                var targetColumn = x % columns_dom.length;
                columns_dom[targetColumn].append($(html));
            }

            $("#searchContainer").prepend(content);

            $.each($.parseJSON(data), function (key, value) {
                nids = value.nid;
                rating = value.rating / 20;
                $('#searchContainer #rat' + nids).raty({
                    readOnly: true,
                    score: rating
                });
            });
        }

        layoutColumns();
        $(window).resize(onResize);

    } else {
        $("#searchContainer").html("<p>Sorry no results found.</p>");
    }

});
/** END **/

$(document).on('vclick', '#searchContainer .card', function (e) {
    var nid = $(this).closest('a').attr('id');
    if (nid == undefined) {
        nid = $(this).find('a').attr('id');
    }
    setStationDetails(nid);
});

$(document).on('pagebeforeshow', '#stations', function (event) {
    $(".stationlist").empty();
});

/** This will fetch the stations for particular category **/
$(document).on('pageshow', '#stations', function (event) {

    $('#stations #category').html(localStorage.getItem('category'));

    var data = localStorage.getItem('stationsFiltered');

    var offline = localStorage.getItem('offlineStatus');
    var sdcardpath = localStorage.getItem('sdcardpath');

    var rating;
    var cards_data = [];
    var cardsObj = {};
    var overview = '';
    var imagepth;

    if (offline == 'on') {
        var regex = new RegExp(filesRoot, "g");
        var data = data.replace(regex, sdcardpath + 'castaway/data/');
    }

    $.each($.parseJSON(data), function (key, value) {

        rating = value.rating / 20;
        overview = value.overview;
        imagepth = value.station_image;

        cardsObj = {
            "stationName": value.station,
            "stationImg": imagepth,
            "nid": value.nid,
            "overview_short": $.trim(overview).substring(0, 100) + "....",
            "rating": rating
        };
        cards_data.push(cardsObj);

    });

    var content, columns, compiledCardTemplate = undefined;
    var MIN_COL_WIDTH = 300;
    content = $(".stationlist");

    compiledCardTemplate = Mustache.compile($("#card-template").html());
    layoutColumns();
    $(window).resize(onResize);

    //resize event handler
    function onResize() {
        var targetColumns = Math.floor($(document).width() / MIN_COL_WIDTH);
        if (columns != targetColumns) {
            layoutColumns();
        }
    }

    function layoutColumns() {

        content.detach();
        content.empty();

        columns = Math.floor($(document).width() / MIN_COL_WIDTH);

        var columns_dom = [];

        for (var x = 0; x < columns; x++) {
            var col = $('<div class="column">');
            col.css("width", Math.floor(100 / columns) + "%");
            columns_dom.push(col);

            content.append(col);
        }

        for (var x = 0; x < cards_data.length; x++) {
            var html = compiledCardTemplate(cards_data[x]);
            var targetColumn = x % columns_dom.length;
            columns_dom[targetColumn].append($(html));
        }

        $("#stationContainer").html('');
        $("#stationContainer").prepend(content);

        $.each($.parseJSON(data), function (key, value) {
            nids = value.nid;
            rating = value.rating / 20;
            $('#stationContainer #rat' + nids).raty({
                readOnly: true,
                score: rating
            });
        });
    }

    $('#stationContainer').prepend('<div id="filterbtn" class="ui-input-search ui-shadow-inset ui-btn-shadow ui-icon-searchfield ui-body-c ui-mini"><input type="text" data-type="search" value="" id="filter" name="filter" placeholder="Filter Places..." data-mini="true" class="ui-input-text ui-body-c"><a title="clear text" class="ui-input-clear ui-btn ui-btn-up-c ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-notext ui-input-clear-hidden" href="#" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-icon="delete" data-iconpos="notext" data-theme="c" data-mini="true"><span class="ui-btn-inner"><span class="ui-btn-text">clear text</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a></div>');
});

$(document).on('vclick', '#stationContainer .card', function (e) {
    var nid = $(this).closest('a').attr('id');
    if (nid == undefined) {
        nid = $(this).find('a').attr('id');
    }
    $('.ui-loader').css('display', 'block');

    var id = localStorage.getItem('id');
    var message = createloading(id);
    $.mobile.loading('show', {
        text: "",
        textVisible: true,
        theme: "a",
        textonly: true,
        html: message
    });
    setStationDetails(nid);
});

$(document).on('vclick', '#stationdetail #overview', function (event) {
    $.mobile.changePage("#overview");
});

$(document).on('vclick', '#rateplace', function (event) {
    $.mobile.changePage("#rateplace");
});

$(document).on('vclick', '#best_season', function (event) {
    $.mobile.changePage("#best_season");
});

$(document).on('vclick', '#weather', function (event) {
    var lat = localStorage.getItem('geolat');
    var lon = localStorage.getItem('geolong');
    if( lat !== '' && lon !== '' ) {
        var weatherurl = localStorage.getItem('weatherapiurl');
        weatherurl = weatherurl.replace('[lat]', lat);
        weatherurl = weatherurl.replace('[lon]', lon);

        $.ajax({
            url: weatherurl,
            type: 'post',
            dataType: 'json',
            beforeSend: function (request) {
                $('body').append("<div class='ui-loader-background'> </div>");
                createloadingmsg('Getting Weather');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            },
            success: function (data) {
                localStorage.removeItem('weatherData');
                localStorage.setItem('weatherData', JSON.stringify(data));
            },
            complete: function () {
                $('.ui-loader').css('display', 'none');
                $.mobile.hidePageLoadingMsg();
                $.mobile.changePage("#weatherpage");
            }
        });
    }else{      
        alert('Error occured while accessing the section, Please try later');
        $.mobile.changePage("#loginpage");
    }
});

$(document).on('vclick', '#shopping', function (event) {
    $.mobile.changePage("#shopping");
});

$(document).on('vclick', '#attractions', function (event) {
    $.mobile.changePage("#attractions");
});

$(document).on('vclick', '#where_to_eat', function (event) {
    $.mobile.changePage("#where_to_eat");
});

$(document).on('vclick', '#how_to_reach', function (event) {
    $.mobile.changePage("#how_to_reach");
});

$(document).on('vclick', '#reviews', function (event) {
    $.mobile.changePage("#reviews");
});

$(document).on('vclick', '#travellerphotos', function (event) {
    $.mobile.changePage("#travellerphotos");
});

/** This will prepare the overview page **/
$(document).on('pageshow', '#overview', function (e) {
    var station_name = localStorage.getItem('title');
    $('#overview h1').html(station_name);
    var overview = localStorage.getItem('overview');
    if ($.trim(overview) == '<br />' || $.trim(overview) == '') {
        overview = "No Data present.";
    }
    $('#overviewtext').html(localStorage.getItem('overview'));
    $('#stationimage').attr('src', localStorage.getItem('station_image'));

    var imgLoad = imagesLoaded('#image-container');
    imgLoad.on('always', function () {

        //Calculation of height of overview image and show loading while loading.
        var view_height = $(window).height();
        var overviewImgHeight = view_height / 2;
        $('#image-container').css('height', overviewImgHeight);
        $('#image-container').addClass('is-loading');

        var loadStatus = imgLoad.images[0].isLoaded;

        if (loadStatus == true) {
            $('#image-container').removeClass('is-loading');
            $('#image-container').css('height', '100%');
        }
    });
});

$(document).on('pageshow', '#rateplace', function (event) {

    var station_name = localStorage.getItem('title');
    $('#rateplace h1').html(station_name);

    var userObj = getUserObject();

    var uid = userObj.user.uid;

    if (userObj !== false) {

        $("#loginlink").hide();
        $("#rateit").show();

        var ratingScore = '';

        $('#rating_review').raty({
            click: function (score, evt) {
                $('#stRating').val(score);
            }
        });
    } else {

    }
});

$(document).on('vclick', '#ratingsubmit', function () {
    var rating_submitted = $('#stRating').val();

    var nid = localStorage.getItem('nid', nid);

    var userObj = getUserObject();

    var uid = userObj.user.uid;

    if (rating_submitted !== '') {
        createloadingmsg('Submitting Rating');
        $.ajax({
            url: baseurl + "/services/session/token",
            type: "get",
            dataType: "text",
            error: function (jqXHR, textStatus, errorThrown) {

            },
            success: function (token) {
                $.ajax({
                    url: baseurl + "/demo/demo",
                    type: 'post',
                    data: 'method=submitRatings&nid=' + nid + '&uid=' + uid + '&rating=' + rating_submitted,
                    dataType: 'json',
                    beforeSend: function (request) {
                        createloadingmsg('Submitting Rating');
                        request.setRequestHeader("X-CSRF-Token", token);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    success: function (data) {
                        if (data.status == true) {
                            $("#ratingHeading").html("Thank You");
                            $("#ratingMessage").html("Thank you for rating this place. We will update this in system shortly.");
                            $("#okRatingsBack").hide();
                            $("#okRatings").show();
                            $("#ratingPopup").popup("open");
                        } else {
                            $("#ratingHeading").html("Error");
                            $("#ratingMessage").html("There is some error occured please try after some time.");
                            $("#okRatingsBack").show();
                            $("#okRatings").hide();
                            $("#ratingPopup").popup("open");
                        }
                    },
                    complete: function () {
                        $('.ui-loader').css('display', 'none');
                        $.mobile.hidePageLoadingMsg();
                    }
                });
            }
        });
    } else {
        $("#ratingHeading").html("Error");
        $("#ratingMessage").html("Please select appropriate rating to rate.");
        $("#okRatingsBack").show();
        $("#okRatings").hide();
        $("#ratingPopup").popup("open");
    }
});

/** This will prepare the best season page **/
$(document).on('pageshow', '#best_season', function (event) {
    var station_name = localStorage.getItem('title');
    $('#best_season h1').html(station_name);
    $('#bestseasontext').html(localStorage.getItem('best_season'));
});

/** This will prepare the shopping page **/
$(document).on('pageshow', '#shopping', function (event) {
    var station_name = localStorage.getItem('title');
    $('#shopping h1').html(station_name);
    var shopping = localStorage.getItem('shopping');
    if ($.trim(shopping) == '<br />' || $.trim(shopping) == '') {
        shopping = "No Data present.";
    }
    $('#shoppingtext').html(shopping);
});

/** This will prepare the attractions page **/
$(document).on('pageshow', '#attractions', function (event) {
    var station_name = localStorage.getItem('title');
    $('#attractions h1').html(station_name);
    var attractions = localStorage.getItem('attractions');
    if ($.trim(attractions) == '<br />' || $.trim(attractions) == '') {
        attractions = "No Data present.";
    }
    $('#attractionstext').html(attractions);
});

/** This will prepare the where to stay page **/
$(document).on('pageshow', '#where_to_stay', function (event) {
    var station_name = localStorage.getItem('title');
    $('#where_to_stay h1').html(station_name);
    var where_to_stay = localStorage.getItem('where_to_stay');
    if ($.trim(where_to_stay) == '<br />' || $.trim(where_to_stay) == '') {
        where_to_stay = "Coming Soon...";
    }
    $('#wheretostaytext').html(where_to_stay);
});

$(document).on('pageshow', '#where_to_eat', function (event) {
    var station_name = localStorage.getItem('title');
    $('#where_to_eat h1').html(station_name);
    var where_to_eat = localStorage.getItem('where_to_eat');
    if ($.trim(where_to_eat) == '<br />' || $.trim(where_to_eat) == '') {
        where_to_eat = "Coming Soon...";
    }
    $('#wheretoeattext').html(where_to_eat);
});

/** This will prepare the how to reach page **/
$(document).on('pageshow', '#how_to_reach', function (event) {
    var station_name = localStorage.getItem('title');
    $('#how_to_reach h1').html(station_name);
    var how_to_reach = localStorage.getItem('how_to_reach');
    if ($.trim(how_to_reach) == '<br />' || $.trim(how_to_reach) == '') {
        how_to_reach = "No data present.";
    }
    $('#howtoreachtext').html(how_to_reach);
});


function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

/** This will prepare the Reviews page **/
$(document).on('pagebeforecreate', '#reviews', function (event) {
    $("#comment-form").hide();
    $("#reviews #loginForm").hide();
});

$(document).on('click', '#commentform', function () {

    var errStr = '';

    if ($.trim($('#postreview #comment').val()) == '') {
        errStr += 'Please specify comment.<br>';
    }

    if (errStr != '') {
        $('#errorReviewsPop #errmsg').html(errStr);
        $("#errorReviewsPop").popup("option", "overlayTheme", "a");
        $("#errorReviewsPop").popup("open");
    } else {

        var ipaddress = '0.0.0.0';
        var nid = localStorage.getItem('nid');
        var name = $.trim($('#postreview #name').val());
        var comment = $.trim($('#postreview #comment').val());
        //var ratingVal = $.trim($('#reviews #stRating').val());

        var userObj = getUserObject();

        $.ajax({
            url: baseurl + "/services/session/token",
            type: "get",
            dataType: "text",
            error: function (jqXHR, textStatus, errorThrown) {

            },
            success: function (token) {
                $.ajax({
                    url: baseurl + "/demo/demo",
                    type: 'post',
                    data: 'method=submitComments&ip=' + ipaddress + '&name=' + name + '&email=' + userObj.user.email + '&comment=' + comment + '&nid=' + nid + '&uid=' + userObj.user.uid,
                    dataType: 'json',
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    beforeSend: function (request) {
                        createloadingmsg('Posting Review');
                        request.setRequestHeader("X-CSRF-Token", token);
                    },
                    success: function (data) {
                        //Comment created with user record
                        $("#postreview #commentPop").popup("option", "overlayTheme", "a");
                        $("#postreview #commentPop").popup("option", "history", false);
                        $("#postreview #commentPop").popup("open");
                    },
                    complete: function () {
                        $('.ui-loader').css('display', 'none');
                        $.mobile.hidePageLoadingMsg();
                    }
                });
            }
        });
    }
    return false;
});

/** This will prepare the Reviews page **/
$(document).on('pageshow', '#reviews', function (event) {

    var station_name = localStorage.getItem('title');
    $('h1').html(station_name);

    var nid = localStorage.getItem('nid');

    var userObj = getUserObject();

    if (userObj == null) {
        $("#comment-form").hide();
        $("#reviews #loginForm").show();
    } else {
        $("#reviews #loginForm").hide();
        $("#comment-form").show();

        //$('#rating_review').raty({ readOnly: false});

        var ratingScore = '';

        $('#rating_review').raty({
            click: function (score, evt) {
                $('#stRating').val(score);
            }
        });

    }

    $.ajax({
        url: baseurl + "/services/session/token",
        type: "get",
        dataType: "text",
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (token) {
            $.ajax({
                url: baseurl + "/demo/demo",
                type: 'get',
                data: 'method=getComments&nid=' + nid,
                dataType: 'json',
                beforeSend: function (request) {
                    createloadingmsg('Getting Reviews');
                    request.setRequestHeader("X-CSRF-Token", token);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                },
                success: function (data) {

                    var commentsArray = [];
                    var commentUserNonApproved = [];
                    var i = 0;
                    $.each(data, function (key, value) {
                        $.each(value, function (kk, vv) {
                            if (vv.status == 1) {
                                commentsArray.push({
                                    uid: key,
                                    body: vv.body,
                                    created: vv.created,
                                    mail: vv.mail,
                                    name: vv.name,
                                    rating: vv.rating,
                                    status: vv.status,
                                    subject: vv.subject
                                });
                            }

                            if (userObj.user.uid == key && vv.status == 0) {
                                commentUserNonApproved.push({
                                    uid: key,
                                    body: vv.body,
                                    created: vv.created,
                                    mail: vv.mail,
                                    name: vv.name,
                                    rating: vv.rating,
                                    status: vv.status,
                                    subject: vv.subject
                                });
                            }
                            i++;
                        });
                    });

                    if (commentsArray.length > 0) {
                        $('#reviewsList').html('');
                        $.each(commentsArray, function (key, value) {
                            $('#reviewsList').append('<li><a id="" href="#review_detail" rel="external" class="ui-link-inherit"><h3 class="ui-li-heading" id="subject">' + value.subject + '</h3><span id="data"><p class="ui-li-desc">' + value.body + '</p></span><p id="posted">Posted At : ' + value.created + ', Posted By : ' + value.name + '</p></a></li>');
                        });
                    } else {
                        $('#reviewsList').html("<li class='nodata'>No reviews present, Be the first to post review of this place</li>");
                    }

                    if (commentUserNonApproved.length > 0) {
                        $.each(commentUserNonApproved, function (key1, value1) {
                            var adminapproval = '<span style="color:red; text-shadow:none; font-weight: normal;">Pending Admin Approval.</span>'
                            $('#reviewsList').append('<li><a id="" href="#review_detail" rel="external" class="ui-link-inherit">' + adminapproval + '<h3 class="ui-li-heading" id="subject">' + value1.subject + '</h3><span id="data"><p class="ui-li-desc">' + value1.body + '</p></span><p id="posted">Posted At : ' + value1.created + ', Posted By : ' + value1.name + '</p></a></li>');
                        });
                    }

                    $('#reviewsList').listview('refresh');
                },
                complete: function () {
                    $('.ui-loader').css('display', 'none');
                    $.mobile.hidePageLoadingMsg();
                }
            });
        }
    });

    /** Action Event when user taps on Category , it should be send to detail page enlisting list of stations **/
    $('#reviewsList').delegate('li', 'tap', function (event) {
        var subject = $(this).find('#subject').text();
        var description = $(this).find('#data .ui-li-desc').text();

        localStorage.setItem('comment_sub', subject);
        localStorage.setItem('comment_desc', description);
    });

});

$(document).on('pageshow', '#postreview', function (event) {
    var userObj = getUserObject();

    if (userObj == null) {
        $("#postreview #comment-form").hide();
        $("#postreview #loginForm").show();
    } else {
        $("#postreview #loginForm").hide();
        $("#postreview #comment-form").show();
    }


});

$(document).on('vclick', '#okComments', function (e) {
    $.mobile.changePage('#reviews');
});

$(document).on('vclick', '#postreviewbtn', function (e) {
    $.mobile.changePage('#postreview');
    e.stopPropagation();
});

/** This will prepare the how to reach page **/
$(document).on('pageshow', '#review_detail', function (event) {
    var station_name = localStorage.getItem('title');
    $('h1').html(station_name);
    $('#review_detail #sub').html(localStorage.getItem('comment_sub'));
    $('#review_detail #body').html(localStorage.getItem('comment_desc'));
});

$(document).on('pagebeforeshow', '#stationdetail', function (event) {
    //$('#PhotoSwipeTarget').html('');
    $('h1').html("");
});

$(document).on('pageshow', '#stationdetail', function (e) {
    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();

    (function (window, Util, PhotoSwipe) {

        var instance;
        var instancefull;

        var nid = localStorage.getItem('nid');

        var cached_data = localStorage.getItem('sd' + nid);

        var data = $.parseJSON(cached_data);

        localStorage.setItem('title', data.title);

        var title = localStorage.getItem('title');

        $('#station_name').html(title);

        var photos = data.photos;

        var photosArray = [];
        $.each(photos, function (key, value) {
            photosArray.push({
                url: value.thumb,
                caption: value.photo
            });
        });

        window.Code.PhotoSwipe.unsetActivateInstance(instance);

        if (photosArray.length == 0) {
            photosArray.push({
                url: 'css/images/no_image.gif',
                caption: '#'
            });
        }

        if (photosArray.length > 0) {

            instance = PhotoSwipe.attach(
                photosArray, {
                    target: window.document.querySelectorAll('#PhotoSwipeTarget')[0],
                    preventHide: true,
                    captionAndToolbarHide: true,
                    getImageSource: function (obj) {
                        return obj.url;
                    },
                    getImageCaption: function (obj) {
                        return obj.caption;
                    }
                }
            );

            instance.addEventHandler(PhotoSwipe.EventTypes.onTouch, function (e) {

                if (e.action === 'tap') {
                    var currentImage = instance.getCurrentImage();

                    if (currentImage.src !== 'css/images/no_image.gif') {

                        $("#galleryPics").html("");
                        $.each(photosArray, function (key, value) {
                            $('#galleryPics').append('<a href="' + value.caption + '" rel="external"><img src="' + value.url + '" alt="' + title + '" /></a>');
                        });
                        $("#galleryPics a").photoSwipe({
                            enableMouseWheel: false,
                            enableKeyboard: false
                        });
                        $('#galleryPics a[href^="' + currentImage.caption + '"]').click();
                        $('#galleryPics').hide();
                    }
                }

            });
            instance.show(0);

        }
    }(window, window.Code.Util, window.Code.PhotoSwipe));

});

$(document).on('click', '.ps-toolbar-close', function () {
    $.mobile.changePage("#stationdetail");
    //window.history.back();
});

$(document).on('pageshow', '#uploadphoto', function (event) {
    if (isCamera == false) {
        $('#uploadphoto #nonative').show();
        $('#uploadphotodiv').hide()
    } else {
        $('#uploadphoto #nonative').hide();
    }
});

$(document).on('pageshow', '#capture', function (event) {
    if (isCamera == false) {
        $('#capture #nonative').show();
        $('#confirmArea').hide();
        $('#photoptions').hide()
    } else {
        $('#capture #nonative').hide();
    }
});

$(document).on('vclick', '#gotogallery', function () {
    $.mobile.changePage("#travellerphotos");
});

$(document).on('vclick', '#reclick', function () {
    capturePhotoWithFile();
});

$(document).on('pagebeforehide', '#travellerphotos', function (event) {
    $('#photomenu').hide();
});

$(document).on('pageshow', '#travellerphotos', function (event) {
    createloadingmsg('Loading Gallery');
    var nid = localStorage.getItem('nid', nid);
    $.ajax({
        url: baseurl + "/services/session/token",
        type: "get",
        dataType: "text",
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (token) {
            $.ajax({
                url: baseurl + "/demo/demo",
                type: 'get',
                data: 'method=getTravellersPhotos&nid=' + nid,
                dataType: 'json',
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                success: function (data) {
                    if (data.length > 0) {
                        var data = JSON.stringify(data);

                        $('#Gallery').html('');
                        $.each($.parseJSON(data), function (key, value) {
                            $('#Gallery').append('<li><a href="' + value.path + '"><img src="' + value.path + '" alt="' + value.title + '" /></a></li>');
                        });
                        isTravPhotos = true;
                    } else {
                        $('#Gallery').html('<li style="width:100%;">No Photos uploaded yet.</li>');
                    }
                },
                complete: function () {
                    $('#photomenu').slideDown(1000);
                    if (isTravPhotos) {
                        (function (window, $, PhotoSwipe) {
                            var options = {};
                            $("#Gallery a").photoSwipe(options);
                        }(window, window.jQuery, window.Code.PhotoSwipe));
                    }
                    $('.ui-loader').css('display', 'none');
                    $.mobile.hidePageLoadingMsg();
                }
            });
        }
    });
});

$(document).on('pageshow', '#planbymonth', function (event) {
    createMonthsList();
});

$(document).on('click', '#monthslist li', function (event) {
    var id = $(this).attr('id');
    var category = $(this).find('h3').text();
    localStorage.setItem('id', id);
    localStorage.setItem('category', category);
    $('.ui-loader').css('display', 'block');
    var id = localStorage.getItem('id');
    var message = createloading(id);
    $.mobile.loading('show', {
        text: "",
        textVisible: true,
        theme: "a",
        textonly: true,
        html: message
    });
    setMonthsStations(id);
});

function setMonthsStations(id) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {
                    $.ajax({
                        url: fileSystem.root.toURL() + 'castaway/data/cache/all_plan_by_month_stations.json',
                        type: 'get',
                        dataType: 'json',
                        success: function (data) {
                            var data = data[id];
                            var jsonstr = JSON.stringify(data);
                            var regex = new RegExp(filesRoot, "g");
                            var url_Replaced = jsonstr.replace(regex, fileSystem.root.toURL() + 'castaway/data');
                            var data = JSON.parse(url_Replaced);
                            localStorage.setItem('monthsObject', url_Replaced);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(errorThrown);
                        },
                        complete: function () {
                            $('.ui-loader').css('display', 'none');
                            $.mobile.hidePageLoadingMsg();
                            $.mobile.changePage("#planbymonthstations");
                        }
                    });
                } else {
                    $.ajax({
                        url: baseurl + "/demo/demo",
                        type: 'get',
                        data: 'method=getPlanByMonth&mid=' + id,
                        dataType: 'json',
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                        },
                        success: function (data) {
                            var data = JSON.stringify(data);
                            localStorage.setItem('monthsObject', data);
                        },
                        complete: function () {
                            $('.ui-loader').css('display', 'none');
                            $.mobile.hidePageLoadingMsg();
                            $.mobile.changePage("#planbymonthstations");
                        }
                    });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });
}

$(document).on('pagebeforeshow', '#planbymonthstations', function (event) {
    $(".monthsStationsList").empty();
    $(".monthslist").empty();
});

$(document).on('pageshow', '#planbymonthstations', function (event) {

    $('#planbymonthstations #category').html(localStorage.getItem('category'));

    var monthsStationsObject = localStorage.getItem('monthsObject');

    var data = JSON.parse(monthsStationsObject);

    var rating;

    var cards_data = [];
    var cardsObj = {};
    var overview = '';

    $.each(data, function (key, value) {

        rating = value.rating / 20;
        overview = value.overview;

        cardsObj = {
            "stationName": value.station,
            "stationImg": value.station_image,
            "nid": value.nid,
            "overview_short": $.trim(overview).substring(0, 100) + "....",
            "rating": rating
        };
        cards_data.push(cardsObj);

    });

    var content, columns, compiledCardTemplate = undefined;
    var MIN_COL_WIDTH = 300;
    content = $(".monthslist");

    function layoutColumns() {

        content.detach();
        content.empty();

        columns = Math.floor($(document).width() / MIN_COL_WIDTH);

        var columns_dom = [];

        for (var x = 0; x < columns; x++) {
            var col = $('<div class="column">');
            col.css("width", Math.floor(100 / columns) + "%");
            columns_dom.push(col);

            content.append(col);
        }

        for (var x = 0; x < cards_data.length; x++) {
            var html = compiledCardTemplate(cards_data[x]);

            var targetColumn = x % columns_dom.length;

            columns_dom[targetColumn].append($(html));
        }

        $("#monthsStationsList").html('');
        $("#monthsStationsList").prepend(content);

        $.each(data, function (key, value) {
            nids = value.nid;
            rating = value.rating / 20;
            $('#monthsStationsList #rat' + nids).raty({
                readOnly: true,
                score: rating
            });
        });
    }

    compiledCardTemplate = Mustache.compile($("#card-template").html());

    layoutColumns();

    $('#monthsStationsList').prepend('<div id="filtermonthbtn" class="ui-input-search ui-shadow-inset ui-btn-shadow ui-icon-searchfield ui-body-c ui-mini"><input type="text" data-type="search" value="" id="filtermonths" name="filtermonths" placeholder="Filter Places..." data-mini="true" class="ui-input-text ui-body-c"><a title="clear text" class="ui-input-clear ui-btn ui-btn-up-c ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-notext ui-input-clear-hidden" href="#" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-icon="delete" data-iconpos="notext" data-theme="c" data-mini="true"><span class="ui-btn-inner"><span class="ui-btn-text">clear text</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a></div>');

    //resize event handler
    function onResize() {
        var targetColumns = Math.floor($(document).width() / MIN_COL_WIDTH);
        if (columns != targetColumns) {
            layoutColumns();
        }
    }

    $(window).resize(onResize);
});


$(document).on('vclick', '#monthsStationsList .card', function (e) {
    var nid = $(this).closest('a').attr('id');
    if (nid == undefined) {
        nid = $(this).find('a').attr('id');
    }
    $('.ui-loader').css('display', 'block');
    var id = localStorage.getItem('id');
    var message = createloading(id);
    $.mobile.loading('show', {
        text: "",
        textVisible: true,
        theme: "a",
        textonly: true,
        html: message
    });
    setStationDetails(nid);
});

$(document).on('change', '#time', function () {
    var time = this.value;
    var parts = time.split("|");
    var timelow = parts[0];
    var timehigh = parts[1];
    localStorage.setItem('timelow', timelow);
    localStorage.setItem('timehigh', timehigh);
    localStorage.setItem('category', timelow + " - " + timehigh + " Hours");
    setStationsByTime(timelow, timehigh);
});

$(document).on('pageshow', '#planbytime', function (event) {
    $('select[name^="time"] option[value="0"]').attr("selected", "selected");
    $('select[name^="time"]').selectmenu('refresh');
});

$(document).on('pagebeforeshow', '#planbytimelist', function (event) {
    $(".stationtimelist").empty();
});

$(document).on('pageshow', '#planbytimelist', function (event) {

    $('#planbytimelist #category').html(localStorage.getItem('category'));

    var timelow = localStorage.getItem('timelow');
    var timehigh = localStorage.getItem('timehigh');
    var cacheKey = timelow + "|" + timehigh;
    var cached_data = localStorage.getItem('getByTimeStations|' + cacheKey);
    var data = JSON.parse(cached_data);

    if (data.length > 0) {
        var rating;

        var cards_data = [];
        var cardsObj = {};
        var overview = '';

        $.each(data, function (key, value) {

            rating = value.rating / 20;
            overview = value.overview;

            cardsObj = {
                "stationName": value.station,
                "stationImg": value.station_image,
                "nid": value.nid,
                "overview_short": $.trim(overview).substring(0, 100) + "....",
                "rating": rating
            };
            cards_data.push(cardsObj);

        });

        var content, columns, compiledCardTemplate = undefined;
        var MIN_COL_WIDTH = 300;
        content = $(".stationtimelist");

        function layoutColumns() {

            content.detach();
            content.empty();

            columns = Math.floor($(document).width() / MIN_COL_WIDTH);

            var columns_dom = [];

            for (var x = 0; x < columns; x++) {
                var col = $('<div class="column">');
                col.css("width", Math.floor(100 / columns) + "%");
                columns_dom.push(col);

                content.append(col);
            }

            for (var x = 0; x < cards_data.length; x++) {
                var html = compiledCardTemplate(cards_data[x]);

                var targetColumn = x % columns_dom.length;

                columns_dom[targetColumn].append($(html));
            }

            $("#stationListTime").html('');
            $("#stationListTime").prepend(content);

            $.each(data, function (key, value) {
                nids = value.nid;
                rating = value.rating / 20;
                $('#stationListTime #rat' + nids).raty({
                    readOnly: true,
                    score: rating
                });
            });
        }

        compiledCardTemplate = Mustache.compile($("#card-template").html());

        layoutColumns();

        $('#stationListTime').prepend('<div id="filtertimebtn" class="ui-input-search ui-shadow-inset ui-btn-shadow ui-icon-searchfield ui-body-c ui-mini"><input type="text" data-type="search" value="" id="filtertime" name="filtertime" placeholder="Filter Places..." data-mini="true" class="ui-input-text ui-body-c"><a title="clear text" class="ui-input-clear ui-btn ui-btn-up-c ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-notext ui-input-clear-hidden" href="#" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-icon="delete" data-iconpos="notext" data-theme="c" data-mini="true"><span class="ui-btn-inner"><span class="ui-btn-text">clear text</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a></div>');

        //resize event handler
        function onResize() {
            var targetColumns = Math.floor($(document).width() / MIN_COL_WIDTH);
            if (columns != targetColumns) {
                layoutColumns();
            }
        }

        $(window).resize(onResize);

    } else {
        $("#stationListTime").html("<p style='text-decoration:none; color: #FFF;'>Sorry no results found.</p>");
    }
});


$(document).on('vclick', '#stationListTime .card', function (e) {
    var nid = $(this).closest('a').attr('id');
    if (nid == undefined) {
        nid = $(this).find('a').attr('id');
    }
    createloadingmsg('Getting Stations');
    setStationDetails(nid);
});

function setStationsByTime(timelow, timehigh) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {
                    localStorage.setItem('offlineStatus', data);
                    getOfflineByTimeList(fileSystem.root.toURL(), timelow, timehigh);
                } else {
                    localStorage.setItem('offlineStatus', data)
                    getByTimeList(timelow, timehigh);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });
}

function getByTimeList(timelow, timehigh) {
    $.ajax({
        url: baseurl + "/demo/demo",
        type: 'get',
        data: 'method=getByTime&timelow=' + parseInt(timelow) + '&timehigh=' + parseInt(timehigh),
        dataType: 'json',
        beforeSend: function (request) {
            createloadingmsg('Loading Places');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $.mobile.changePage("#stationdetail");
        },
        success: function (data) {
            localStorage.removeItem('getByTimeStations');

            //Setting cache for keys.
            var timelow = localStorage.getItem('timelow');
            var timehigh = localStorage.getItem('timehigh');
            var cacheKey = timelow + "|" + timehigh;
            localStorage.setItem('getByTimeStations|' + cacheKey, JSON.stringify(data));
        },
        complete: function () {
            $('.ui-loader').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
            $.mobile.changePage("#planbytimelist");
        }
    });
}

function getOfflineByTimeList(filepath, timelow, timehigh) {
    $.ajax({
        url: filepath + 'castaway/data/cache/all_time_to_reach.json',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var filtered_stations = [];
            $.each(data, function (key, value) {
                if(value.time_to_reach !== null){
                    if((value.time_to_reach >= parseInt(timelow)) && (value.time_to_reach <= parseInt(timehigh))) {
                        filtered_stations.push(value);
                    }
                }
            });
            
            localStorage.removeItem('getByTimeStations');
            var data_filtered_stations = JSON.stringify(filtered_stations);
            var regex = new RegExp(filesRoot, "g");
            var url_Replaced = data_filtered_stations.replace(regex, fileSystem.root.toURL() + 'castaway/data/');
            var cacheKey = timelow + "|" + timehigh;
            localStorage.setItem('getByTimeStations|' + cacheKey, url_Replaced);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
            $.mobile.changePage("#homepage");
        },
        complete: function () {
            $('.ui-loader').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
            $.mobile.changePage("#planbytimelist");
        }
    });
}

$(document).on('pageshow', '#showmap', function (event) {

    var lat = localStorage.getItem('latitude');
    var lng = localStorage.getItem('longitude');

    var latlng = new google.maps.LatLng(lat, lng);

    var options = {
        zoom: 15,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var $content = $("#locmap");

    $content.height(screen.height - 50);

    var map = new google.maps.Map($content[0], options);

    new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        position: latlng
    });

    $('.ui-loader').css('display', 'none');
    $.mobile.hidePageLoadingMsg();
});

$(document).on('vclick', '#setLocation', function (e) {
    $.mobile.changePage("#nearestgateway");
    e.preventDefault();
});

$(document).on('pageshow', '#nearestgateway', function (event) {

    if(localStorage.getItem('default_distance') == null) {
        localStorage.setItem('default_distance', '100');
    }

    var latitude = localStorage.getItem('latitude');
    var longitude = localStorage.getItem('longitude');
    var radius = localStorage.getItem('default_distance');

    cordova.plugins.diagnostic.isLocationEnabled(function(status){
        if( status == 0){
            cordova.plugins.diagnostic.switchToLocationSettings();
        }
    }, function(error){
        alert("The following error occurred: "+error);
    });    

    if (latitude == null && longitude == null) {
        var latitude = '19.076'; //Default latitude of mumbai;
        var longitude = '72.8777'; // Default Longitude of mumbai;
    }
    loadNearestStations(latitude, longitude, radius);
});

$(document).on('click', '#set', function (event) {
    var distance = $('#distance').val();
    var latitude = localStorage.getItem('latitude');
    var longitude = localStorage.getItem('longitude');
    localStorage.setItem('default_distance', distance);
    loadNearestStations(latitude, longitude, distance);
    $("#popupBasic").popup("close");
});

$(document).on('click', '#updategps', function (event) {
    createloadingmsg('Finding Your Location');
    forceGpsSet();
});

function loadNearestStations(latitude, longitude, radius) {

    var latitude_stored = localStorage.getItem('latitude');
    var longitude_stored = localStorage.getItem('longitude');

    var distFromPrevPoints = distance(latitude_stored, longitude_stored, latitude, longitude, 'K');

    var firsttimecache = localStorage.getItem('locationCache');


    if (distFromPrevPoints >= 8 || firsttimecache == null) {

        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=getNearestStations&lat=' + latitude + '&lng=' + longitude + '&radius=' + radius,
            dataType: 'json',
            beforeSend: function (request) {
                createloadingmsg('Searching Nearest Locations');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            },
            success: function (data) {

                var rating;

                var cards_data = [];
                var cardsObj = {};
                var overview = '';

                $.each(data, function (key, value) {

                    rating = value.rating / 20;
                    overview = value.station_overview;

                    cardsObj = {
                        "stationName": value.station,
                        "stationImg": value.station_image_thumb,
                        "nid": value.nid,
                        "overview_short": $.trim(overview).substring(0, 100) + "....",
                        "rating": rating
                    };
                    cards_data.push(cardsObj);

                });

                localStorage.setItem('locationCache', JSON.stringify(data));

                var content, columns, compiledCardTemplate = undefined;
                var MIN_COL_WIDTH = 300;
                content = $(".nearestStations");

                function layoutColumns() {

                    content.detach();
                    content.empty();

                    columns = Math.floor($(document).width() / MIN_COL_WIDTH);

                    var columns_dom = [];

                    for (var x = 0; x < columns; x++) {
                        var col = $('<div class="column">');
                        col.css("width", Math.floor(100 / columns) + "%");
                        columns_dom.push(col);

                        content.append(col);
                    }

                    for (var x = 0; x < cards_data.length; x++) {
                        var html = compiledCardTemplate(cards_data[x]);

                        var targetColumn = x % columns_dom.length;

                        columns_dom[targetColumn].append($(html));
                    }

                    $("#nearestStationsList").prepend(content);

                    $.each(data, function (key, value) {
                        nids = value.nid;
                        rating = value.rating / 20;
                        $('#nearestStationsList #rat' + nids).raty({
                            readOnly: true,
                            score: rating
                        });
                    });
                }

                compiledCardTemplate = Mustache.compile($("#card-template").html());

                layoutColumns();

                //resize event handler
                function onResize() {
                    var targetColumns = Math.floor($(document).width() / MIN_COL_WIDTH);
                    if (columns != targetColumns) {
                        layoutColumns();
                    }
                }

                $(window).resize(onResize);

            },
            complete: function () {
                $('.ui-loader').css('display', 'none');
                $.mobile.hidePageLoadingMsg();
            }
        });
    } else {

        var data = JSON.parse(localStorage.getItem('locationCache'));

        var rating;

        var cards_data = [];
        var cardsObj = {};
        var overview = '';

        $.each(data, function (key, value) {

            rating = value.rating / 20;
            overview = value.station_overview;

            cardsObj = {
                "stationName": value.station,
                "stationImg": value.station_image_thumb,
                "nid": value.nid,
                "overview_short": $.trim(overview).substring(0, 100) + "....",
                "rating": rating
            };
            cards_data.push(cardsObj);

        });

        var content, columns, compiledCardTemplate = undefined;
        var MIN_COL_WIDTH = 300;
        content = $(".nearestStations");

        function layoutColumns() {

            content.detach();
            content.empty();

            columns = Math.floor($(document).width() / MIN_COL_WIDTH);

            var columns_dom = [];

            for (var x = 0; x < columns; x++) {
                var col = $('<div class="column">');
                col.css("width", Math.floor(100 / columns) + "%");
                columns_dom.push(col);

                content.append(col);
            }

            for (var x = 0; x < cards_data.length; x++) {
                var html = compiledCardTemplate(cards_data[x]);

                var targetColumn = x % columns_dom.length;

                columns_dom[targetColumn].append($(html));
            }

            $("#nearestStationsList").prepend(content);

            $.each(data, function (key, value) {
                nids = value.nid;
                rating = value.rating / 20;
                $('#rat' + nids).raty({
                    readOnly: true,
                    score: rating
                });
            });
        }

        compiledCardTemplate = Mustache.compile($("#card-template").html());

        layoutColumns();

        //resize event handler
        function onResize() {
            var targetColumns = Math.floor($(document).width() / MIN_COL_WIDTH);
            if (columns != targetColumns) {
                layoutColumns();
            }
        }

        $(window).resize(onResize);

        $('.ui-loader').css('display', 'none');
        $.mobile.hidePageLoadingMsg();

    }
}

$(document).on('vclick', '#nearestStationsList .card', function (e) {
    var nid = $(this).closest('a').attr('id');
    if (nid == undefined) {
        nid = $(this).find('a').attr('id');
    }
    setStationDetails(nid);
});

$(document).on('pageshow', '#weatherpage', function (event) {

    var title = localStorage.getItem('title');
    $('#weatherpage #station_name2').html(title);

    var data = JSON.parse(localStorage.getItem('weatherData'));

    var temp = data.main.temp - 273.15;
    var humidity = data.main.humidity;
    var pressure = data.main.pressure;

    var sunrise = formatTime(data.sys.sunrise);
    var sunset = formatTime(data.sys.sunset);

    var weatherdesc = data.weather[0].description;
    var icon = data.weather[0].icon;

    var speed = data.wind.speed;

    $('#weathercond').html(weatherdesc);

    $('#temp').html(parseInt(temp) + '&deg;C');
    $('#humidity').html(humidity + ' %');
    $('#pressure').html(pressure + ' hpa');

    $('#sunrise').html(sunrise);
    $('#sunset').html(sunset);

    $('#speed').html(speed + ' m/s');

    var ico = 'http://openweathermap.org/img/w/' + icon + '.png';

    $('#weathericon').prop('src', ico);

});

var formatTime = function (unixTimestamp) {
    var dt = new Date(unixTimestamp * 1000);

    var hours = dt.getHours();
    var minutes = dt.getMinutes();
    var seconds = dt.getSeconds();

    // the above dt.get...() functions return a single digit
    // so I prepend the zero here when needed
    if (hours < 10)
        hours = '0' + hours;

    if (minutes < 10)
        minutes = '0' + minutes;

    if (seconds < 10)
        seconds = '0' + seconds;

    return hours + ":" + minutes + ":" + seconds;
}

/** PLAN BY ATTRACTION LOGIC STARTS HERE **/

$(document).on('pageshow', '#planbyattractions', function (event) {
    createAttractionsList();
});

/** Action Event when user taps on Category , it should be send to detail page enlisting list of stations **/
$(document).on('click', '#attractionsList li', function (event) {
    var id = $(this).attr('id');
    localStorage.setItem('id', id);
    var category = $(this).find('h3').text();
    localStorage.setItem('category', category);
    $('.ui-loader').css('display', 'block');
    var id = localStorage.getItem('id');
    var message = createloading(id);
    $.mobile.loading('show', {
        text: "",
        textVisible: true,
        theme: "a",
        textonly: true,
        html: message
    });
    setAttractionStations(id);
});

$(document).on('pagebeforeshow', '#attractionsStationList', function (event) {
    $(".attractionslist").empty();
});

$(document).on('pageshow', '#attractionsStationList', function (event) {

    $('#attractionsStationList #category').html(localStorage.getItem('category'));

    var data = JSON.parse(localStorage.getItem('attractionsStations'));

    var rating;

    var cards_data = [];
    var cardsObj = {};
    var overview = '';

    $.each(data, function (key, value) {

        rating = value.rating / 20;
        overview = value.overview;

        cardsObj = {
            "stationName": value.station,
            "stationImg": value.station_image,
            "nid": value.nid,
            "overview_short": $.trim(overview).substring(0, 100) + "....",
            "rating": rating
        };
        cards_data.push(cardsObj);

    });

    var content, columns, compiledCardTemplate = undefined;
    var MIN_COL_WIDTH = 300;
    content = $(".attractionslist");

    function layoutColumns() {

        content.detach();
        content.empty();

        columns = Math.floor($(document).width() / MIN_COL_WIDTH);

        var columns_dom = [];

        for (var x = 0; x < columns; x++) {
            var col = $('<div class="column">');
            col.css("width", Math.floor(100 / columns) + "%");
            columns_dom.push(col);

            content.append(col);
        }

        for (var x = 0; x < cards_data.length; x++) {
            var html = compiledCardTemplate(cards_data[x]);

            var targetColumn = x % columns_dom.length;

            columns_dom[targetColumn].append($(html));
        }

        $("#attractionsDataList").html('');
        $("#attractionsDataList").prepend(content);

        $.each(data, function (key, value) {
            nids = value.nid;
            rating = value.rating / 20;
            $('#attractionsStationList #rat' + nids).raty({
                readOnly: true,
                score: rating
            });
        });
    }

    compiledCardTemplate = Mustache.compile($("#card-template").html());

    layoutColumns();

    $('#attractionsDataList').prepend('<div id="filterattractionbtn" class="ui-input-search ui-shadow-inset ui-btn-shadow ui-icon-searchfield ui-body-c ui-mini"><input type="text" data-type="search" value="" id="filterattractions" name="filterattractions" placeholder="Filter Places..." data-mini="true" class="ui-input-text ui-body-c"><a title="clear text" class="ui-input-clear ui-btn ui-btn-up-c ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-notext ui-input-clear-hidden" href="#" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-icon="delete" data-iconpos="notext" data-theme="c" data-mini="true"><span class="ui-btn-inner"><span class="ui-btn-text">clear text</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a></div>');

    //resize event handler
    function onResize() {
        var targetColumns = Math.floor($(document).width() / MIN_COL_WIDTH);
        if (columns != targetColumns) {
            layoutColumns();
        }
    }

    $(window).resize(onResize);
});

$(document).on('vclick', '#attractionsDataList .card', function (e) {
    var nid = $(this).closest('a').attr('id');
    if (nid == undefined) {
        nid = $(this).find('a').attr('id');
    }
    $('.ui-loader').css('display', 'block');
    var id = localStorage.getItem('id');
    var message = createloading(id);
    $.mobile.loading('show', {
        text: "",
        textVisible: true,
        theme: "a",
        textonly: true,
        html: message
    });
    setStationDetails(nid);
});

function setAttractionStations(acid) {
    if (acid != '') {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
            $.ajax({
                url: filepath,
                type: 'get',
                dataType: 'text',
                success: function (data) {
                    if (data == 'on') {
                        getOfflineAttractionStations(fileSystem.root.toURL(), acid);
                    } else {
                        getAttractionStations(acid);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                },
            });
        });
    }
}

function getOfflineAttractionStations(filepath, acid) {
    $.ajax({
        url: filepath + 'castaway/data/cache/all_plan_by_attractions.json',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var data = data[acid];
            data = JSON.stringify(data);
            var regex = new RegExp(filesRoot, "g");
            var url_Replaced = data.replace(regex, fileSystem.root.toURL() + 'castaway/data/');
            localStorage.removeItem('attractionsStations');
            localStorage.setItem('attractionsStations', url_Replaced);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
            $.mobile.changePage("#homepage");
        },
        complete: function () {
            $('.ui-loader').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
            $.mobile.changePage("#attractionsStationList");
        }
    });
}

function getAttractionStations(acid) {
    $.ajax({
        url: baseurl + "/demo/demo",
        type: 'get',
        data: 'method=getStationsByAttractions&sid=' + parseInt(acid),
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $.mobile.changePage("#homepage");
        },
        success: function (data) {
            localStorage.removeItem('attractionsStations');
            localStorage.setItem('attractionsStations', JSON.stringify(data));
        },
        complete: function () {
            $('.ui-loader').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
            $.mobile.changePage("#attractionsStationList");
        }
    });
}

/** PLAN BY ATTRACTION LOGIC ENDS HERE **/

/** PLAN BY REGIONS LOGIC STARTS HERE **/
$(document).on('pageshow', '#planbyregions', function (event) {
    createRegionsList();
});

/** Action Event when user taps on Category , it should be send to detail page enlisting list of stations **/
$(document).on('click', '#religionsList li', function (event) {
    var id = $(this).attr('id');
    localStorage.setItem('id', id);
    var category = $(this).find('h3').text();
    localStorage.setItem('category', category);
    $('.ui-loader').css('display', 'block');
    var id = localStorage.getItem('id');
    var message = createloading(id);
    $.mobile.loading('show', {
        text: "",
        textVisible: true,
        theme: "a",
        textonly: true,
        html: message
    });
    setRegionStations(id);
});

$(document).on('pagebeforeshow', '#regionsStationList', function (event) {
    $(".regionslist").empty();
});

$(document).on('pageshow', '#regionsStationList', function (event) {

    $('#regionsStationList #category').html(localStorage.getItem('category'));

    var data = JSON.parse(localStorage.getItem('regionsStations'));

    var rating;

    var cards_data = [];
    var cardsObj = {};
    var overview = '';

    $.each(data, function (key, value) {

        rating = value.rating / 20;
        overview = value.overview;

        cardsObj = {
            "stationName": value.station,
            "stationImg": value.station_image,
            "nid": value.nid,
            "overview_short": $.trim(overview).substring(0, 100) + "....",
            "rating": rating
        };
        cards_data.push(cardsObj);

    });

    var content, columns, compiledCardTemplate = undefined;
    var MIN_COL_WIDTH = 300;
    content = $(".regionslist");

    function layoutColumns() {

        content.detach();
        content.empty();

        columns = Math.floor($(document).width() / MIN_COL_WIDTH);

        var columns_dom = [];

        for (var x = 0; x < columns; x++) {
            var col = $('<div class="column">');
            col.css("width", Math.floor(100 / columns) + "%");
            columns_dom.push(col);

            content.append(col);
        }

        for (var x = 0; x < cards_data.length; x++) {
            var html = compiledCardTemplate(cards_data[x]);

            var targetColumn = x % columns_dom.length;

            columns_dom[targetColumn].append($(html));
        }

        $("#regionsDataList").html('');
        $("#regionsDataList").prepend(content);

        $.each(data, function (key, value) {
            nids = value.nid;
            rating = value.rating / 20;
            $('#regionsDataList #rat' + nids).raty({
                readOnly: true,
                score: rating
            });
        });
    }

    compiledCardTemplate = Mustache.compile($("#card-template").html());

    layoutColumns();

    $('#regionsDataList').prepend('<div id="filterregionsbtn" class="ui-input-search ui-shadow-inset ui-btn-shadow ui-icon-searchfield ui-body-c ui-mini"><input type="text" data-type="search" value="" id="filterregions" name="filterregions" placeholder="Filter Places..." data-mini="true" class="ui-input-text ui-body-c"><a title="clear text" class="ui-input-clear ui-btn ui-btn-up-c ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-notext ui-input-clear-hidden" href="#" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-icon="delete" data-iconpos="notext" data-theme="c" data-mini="true"><span class="ui-btn-inner"><span class="ui-btn-text">clear text</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a></div>');

    //resize event handler
    function onResize() {
        var targetColumns = Math.floor($(document).width() / MIN_COL_WIDTH);
        if (columns != targetColumns) {
            layoutColumns();
        }
    }

    $(window).resize(onResize);

});

$(document).on('vclick', '#regionsDataList .card', function (e) {
    var nid = $(this).closest('a').attr('id');
    if (nid == undefined) {
        nid = $(this).find('a').attr('id');
    }
    $('.ui-loader').css('display', 'block');
    var id = localStorage.getItem('id');
    var message = createloading(id);
    $.mobile.loading('show', {
        text: "",
        textVisible: true,
        theme: "a",
        textonly: true,
        html: message
    });
    setStationDetails(nid);
});

function setRegionStations(rcid) {
    if (rcid != '') {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {                    
                    $.ajax({
                        url: fileSystem.root.toURL() + 'castaway/data/cache/all_by_regions.json',
                        type: 'get',
                        dataType: 'json',
                        success: function (data) {
                            var data = data[rcid];
                            var jsonstr = JSON.stringify(data);
                            var regex = new RegExp(filesRoot, "g");
                            var url_Replaced = jsonstr.replace(regex, fileSystem.root.toURL() + 'castaway/data');
                            localStorage.removeItem('regionsStations');
                            localStorage.setItem('regionsStations', url_Replaced);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(errorThrown);
                        },
                        complete: function () {
                            $('.ui-loader').css('display', 'none');
                            $.mobile.hidePageLoadingMsg();
                            $.mobile.changePage("#regionsStationList");
                        }
                    });
                } else {
                    $.ajax({
                        url: baseurl + "/demo/demo",
                        type: 'get',
                        data: 'method=getStationsByRegions&sid=' + parseInt(rcid),
                        dataType: 'json',
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $.mobile.changePage("#stationdetail");
                        },
                        success: function (data) {
                            localStorage.removeItem('regionsStations');
                            localStorage.setItem('regionsStations', JSON.stringify(data));
                        },
                        complete: function () {
                            $('.ui-loader').css('display', 'none');
                            $.mobile.hidePageLoadingMsg();
                            $.mobile.changePage("#regionsStationList");
                        }
                    });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });
    }
}
/** PLAN BY REGIONS LOGIC ENDS HERE **/

function setStationDetails(nid) {

    localStorage.setItem('nid', nid);

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var filepath = fileSystem.root.toURL() + "castaway/offline.txt";
        $.ajax({
            url: filepath,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data == 'on') {
                    localStorage.setItem('offlineStatus', data);
                    getOfflineStationsdetails(fileSystem.root.toURL(), nid);
                } else {
                    localStorage.setItem('offlineStatus', data)
                    getStationsdetails(nid);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        });
    });
}

function getOfflineStationsdetails(filepath, nid) {
    $.ajax({
        url: filepath + 'castaway/data/cache/all_station_details.json',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var data = data[nid];
            var jsonstr = JSON.stringify(data);
            var regex = new RegExp(filesRoot, "g");
            var url_Replaced = jsonstr.replace(regex, filepath + 'castaway/data');

            var data = JSON.parse(url_Replaced);

            localStorage.setItem('nid', data.nid);
            localStorage.setItem('station_image', data.station_image);
            localStorage.setItem('rating', data.rating);
            localStorage.setItem('overview', data.overview);
            localStorage.setItem('best_season', data.best_season);
            localStorage.setItem('shopping', data.shopping);
            localStorage.setItem('attractions', data.attractions);
            localStorage.setItem('where_to_stay', data.where_to_stay);
            localStorage.setItem('where_to_eat', data.where_to_eat);
            localStorage.setItem('how_to_reach', data.how_to_reach);
            localStorage.setItem('geolat', data.geolat);
            localStorage.setItem('geolong', data.geolong);

            localStorage.setItem('sd' + nid, url_Replaced);

            $('.ui-loader').css('display', 'none');
            $.mobile.hidePageLoadingMsg();
            $.mobile.changePage("#stationdetail");
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        },
    });
}

function getStationsdetails(nid) {
    var cached_data = localStorage.getItem('sd' + nid);
    //IF First time user has opened station detail.
    if (cached_data == null) {
        $.ajax({
            url: baseurl + "/demo/demo",
            type: 'get',
            data: 'method=getStationDetails&nid=' + nid,
            dataType: 'json',
            beforeSend: function (request) {
                $('.ui-loader').css('display', 'block');
                var id = localStorage.getItem('id');
                var message = createloading(id);
                $.mobile.loading('show', {
                    text: "",
                    textVisible: true,
                    theme: "a",
                    textonly: true,
                    html: message
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            },
            success: function (data) {
                localStorage.setItem('nid', data.nid);
                localStorage.setItem('station_image', data.station_image);
                localStorage.setItem('rating', data.rating);
                localStorage.setItem('overview', data.overview);
                localStorage.setItem('best_season', data.best_season);
                localStorage.setItem('shopping', data.shopping);
                localStorage.setItem('attractions', data.attractions);
                localStorage.setItem('where_to_stay', data.where_to_stay);
                localStorage.setItem('where_to_eat', data.where_to_eat);
                localStorage.setItem('how_to_reach', data.how_to_reach);
                localStorage.setItem('geolat', data.geolat);
                localStorage.setItem('geolong', data.geolong);

                var data = JSON.stringify(data);
                localStorage.setItem('sd' + nid, data);
            },
            complete: function () {
                $('.ui-loader').css('display', 'none');
                $.mobile.hidePageLoadingMsg();
                $.mobile.changePage("#stationdetail");
            }
        });
    } else {
        $('.ui-loader').css('display', 'block');
        var id = localStorage.getItem('id');
        var message = createloading(id);
        $.mobile.loading('show', {
            text: "",
            textVisible: true,
            theme: "a",
            textonly: true,
            html: message
        });
        $.mobile.changePage("#stationdetail");
    }
}

function getTip(messages) {
    var random_no = Math.floor(Math.random() * messages.length);
    var randommsg = messages[random_no];
    $('.loadermsg').html(randommsg);
    return randommsg;
}

function createloadingmsg(msg) {
    if (typeof(msg) === 'undefined') msg = "Please Wait";
    $('.ui-loader').css('display', 'block');
    var message = '';
    message += '<div class="loaderhor"></div>';
    message += '<div class="loaderhead">' + msg + '...</div>';
    $.mobile.loading('show', {
        text: "",
        textVisible: true,
        theme: "a",
        textonly: true,
        html: message
    });
}

function createloading(id) {
    var tipsdata = localStorage.getItem('tip-' + id);
    var messages = [];
    if (tipsdata == 'null') {
        var messages = [
            "Always keep umbrella with you while travelling to hill stations",
            "Avoid wearing heels and slippers in hill areas",
            "Keep torch with you when going for walk in beach at night"
        ];
    } else {
        var messages = tipsdata.split('\n');
    }
    // If interval is set then clear it.
    if (interval) {
        clearInterval(interval);
    }
    interval = setInterval(function () {
        getTip(messages)
    }, 1400);
    var random_no = Math.floor(Math.random() * messages.length);
    var randommsg = messages[random_no];

    var html = '';
    html += '<div class="loaderhor"></div>';
    html += '<div class="loaderhead">Travel Tips </div>';
    html += '<div class="loadermsg">' + randommsg + '</div>';
    return html;
}
/** END of javascript file **/