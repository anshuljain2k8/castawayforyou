var cb = new Codebird; // we will require this everywhere
var interval; // for setting intervals.

function loginTwitter(){
	cb.setConsumerKey("WhHqzPh37B3d2iA9O65uzFF2X", "Ba7T3JUjoYCpXZXlj43IONTrrVY6iiJeSOeDuCdXcEcKExMhxN");
	var id;
	// check if we already have access tokens
	if(localStorage.accessToken && localStorage.tokenSecret) {
		// then directly setToken() and read the timeline
		cb.setToken(localStorage.accessToken, localStorage.tokenSecret);		
		cb.__call(
			"account_verifyCredentials", {},
			function (reply) {				
				localStorage.setItem('twitterdetails', JSON.stringify(reply));
				$('.ui-loader').css('display', 'block');
				$.mobile.showPageLoadingMsg('a', 'Logging In...');
				$.mobile.changePage('#homepage');
			}						
		);	
	} else { // authorize the user and ask her to get the pin.
		cb.__call(
			"oauth_requestToken",
    			{oauth_callback: "http://localhost/"},
    			function (reply) {
					// nailed it!
    	   			cb.setToken(reply.oauth_token, reply.oauth_token_secret);
    	   			cb.__call(
        			"oauth_authorize",	{},
        			function (auth_url) {
        				var ref = window.open(auth_url, '_blank', 'location=no'); // redirection.
        				// check if the location the phonegap changes to matches our callback url or not
        				ref.addEventListener("loadstart", function(iABObject) {
        					if(iABObject.url.match(/localhost/)) {
        						ref.close();								
        						authorize(iABObject);
        					}
        				});        					
	       			}
				);
       		}
		);
	}
}

function authorize(o) {
	var currentUrl = o.url;
	var query = currentUrl.match(/oauth_verifier(.+)/);
   	for (var i = 0; i < query.length; i++) {
    	parameter = query[i].split("=");
    	if (parameter.length === 1) {
        	parameter[1] = "";
    	}
	}
	cb.__call(
       	"oauth_accessToken", {oauth_verifier: parameter[1]},
       	function (reply) {
    	   	cb.setToken(reply.oauth_token, reply.oauth_token_secret);
           	localStorage.accessToken = reply.oauth_token;
           	localStorage.tokenSecret = reply.oauth_token_secret;
           	cb.__call(
				"account_verifyCredentials", {},
				function (reply) {					
					localStorage.setItem('twitterdetails', JSON.stringify(reply));
					$('.ui-loader').css('display', 'block');
					$.mobile.showPageLoadingMsg('a', 'Logging In...');
					$.mobile.changePage('#homepage');
				}						
			);
        }
    );    
}