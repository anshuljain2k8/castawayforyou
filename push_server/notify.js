﻿var gcm = require('node-gcm');
var message = new gcm.Message();
//API Server Key
var sender = new gcm.Sender('AIzaSyBKWGFyKtD08bEzUOAqBHFLkR9i');
var registrationIds = [];
// Value the payload data to send...
message.addData('message',"\u270C Peace, Love \u2764 and PhoneGap \u2706!");
message.addData('title','Push Notification Sample' );
message.addData('msgcnt','3'); // Shows up in the notification in the status bar
message.addData('soundname','beep.wav'); //Sound to play upon notification receipt - put in the www folder in app
//message.collapseKey = 'demo';
//message.delayWhileIdle = true; //Default is false
message.timeToLive = 3000;// Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.
// At least one reg id required
registrationIds.push('APA91bGIGeD96IRYu5UZZ_IgZECeXe5B93LPhMpBU4yXaF3zvQ2x3ua8xp4jbAaZXX6wthxzOjHEJa9F3Bg60wGpv5hZyQRyK9QO4Uce1q_Xtcs3MqTwGJTYOET_XKMYRcNeRFSWK4jpXDKlCQ_FNvpzy9PF0d1R7Q');

/**
* Parameters: message-literal, registrationIds-array, No. of retries, callback-function
*/
sender.send(message, registrationIds, 4, function (result) {
console.log(result);
});