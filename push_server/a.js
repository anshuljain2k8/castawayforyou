var gcm = require('node-gcm');

// create a message with default values
var message = new gcm.Message();

// or with object values
var message = new gcm.Message({
    collapseKey: 'demo',
    delayWhileIdle: true,
    timeToLive: 3,
    data: {
        key1: 'message1',
        key2: 'message2'
    }
});

var sender = new gcm.Sender('AIzaSyBKWGFyKtD08bEzUOAqBHFLkR9i');
var registrationIds = [];

// or with backwards compability of previous versions
message.addData('key1','message1');

message.collapseKey = 'demo';
message.delayWhileIdle = true;
message.timeToLive = 3;
message.dryRun = true;
// END OPTIONAL

// At least one required
registrationIds.push('APA91bGIGeD96IRYu5UZZ_IgZECeXe5B93LPhMpBU4yXaF3zvQ2x3ua8xp4jbAaZXX6wthxzOjHEJa9F3Bg60wGpv5hZyQRyK9QO4Uce1q_Xtcs3MqTwGJTYOET_XKMYRcNeRFSWK4jpXDKlCQ_FNvpzy9PF0d1R7Q');

/**
 * Params: message-literal, registrationIds-array, No. of retries, callback-function
 **/
sender.send(message, registrationIds, 4, function (err, result) {
    console.log(result);
});